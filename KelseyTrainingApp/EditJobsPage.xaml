﻿<Page x:Class="KelseyTrainingApp.EditJobsPage"
      xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
      xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
      xmlns:local="clr-namespace:KelseyTrainingApp"
      mc:Ignorable="d" 
      d:DesignHeight="600" d:DesignWidth="660"
      Title="Edit Jobs">

    <!-- Stylish Flat Buttons!! -->
    <Page.Resources>
        <Style BasedOn="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" TargetType="Button">
            <Style.Triggers>
                <Trigger Property="IsEnabled" Value="True">
                    <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlLightLightBrushKey}}" />
                </Trigger>
                <Trigger Property="IsEnabled" Value="False">
                    <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlDarkBrushKey}}" />
                </Trigger>
            </Style.Triggers>
        </Style>
        <Style BasedOn="{StaticResource {x:Static ToolBar.ToggleButtonStyleKey}}" TargetType="ToggleButton">
            <Style.Triggers>
                <Trigger Property="IsEnabled" Value="True">
                    <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlLightLightBrushKey}}" />
                </Trigger>
                <Trigger Property="IsEnabled" Value="False">
                    <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlDarkBrushKey}}" />
                </Trigger>
            </Style.Triggers>
        </Style>
    </Page.Resources>

    <Grid>
        <Grid.Background>
            <SolidColorBrush Color="{DynamicResource {x:Static SystemColors.WindowColorKey}}"/>
        </Grid.Background>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />            <!-- Deletion Note Text Block -->
            <RowDefinition Height="50" />              <!-- Edit Filter -->
            <RowDefinition Height="1*" />              <!-- Edit Grid -->
            <RowDefinition Height="auto" />            <!-- Toolbar -->
            <RowDefinition Height="40" />              <!-- Edit Textbox -->
        </Grid.RowDefinitions>

        <!-- Deletion Note -->
        <Grid Grid.Row="0">
            <TextBlock HorizontalAlignment="Left" VerticalAlignment="Center" TextWrapping="Wrap">
                To delete a job:  Click Edit to bring it to the bottom panel so it can be modified.  Click the 'Delete Job' button.
                <LineBreak />
                -- NOTE: A Job can only be deleted from the database when NO EMPLOYEES are assigned to it.  
                <LineBreak /><LineBreak />
                If you accidently create a new Job with an incorrect Name (typo, misspelled), you can safely delete it here because no employees will have been assigned to it yet.
                <LineBreak /><LineBreak />
                If you need to delete a job (ex. the job is no longer used), edit the employees to different jobs then delete the job from here.
            </TextBlock>
        </Grid>

        <!-- Filter Box -->
        <GroupBox Grid.Row="1" Header="Filter: ">
            <TextBox Name="FilterBox" Height="22" TextChanged="FilterBox_TextChanged" />
        </GroupBox>

        <!-- DataGrid -->
        <DataGrid Grid.Row="2" Name="Grid" AutoGenerateColumns="False" CanUserAddRows="False" IsReadOnly="True" 
                  CanUserSortColumns="True" AlternatingRowBackground="#559DBDC6" ItemsSource="{Binding}"
                  SelectionChanged="Grid_SelectionChanged" SelectionMode="Single">
            <DataGrid.Columns>
                <DataGridTextColumn Binding="{Binding ID}" Width="50" Header="ID" />
                <DataGridTextColumn Binding="{Binding Name}" Width="1*" Header="Name" SortMemberPath="Name" />
                <!-- TODO Add column with names of some employees assigned to each Item? -->
            </DataGrid.Columns>
        </DataGrid>

        <!-- Toolbar -->
        <Grid Grid.Row="3" HorizontalAlignment="Stretch" Margin="0,4">
            <Grid.Background>
                <SolidColorBrush Color="{DynamicResource {x:Static SystemColors.ControlDarkDarkColorKey}}"/>
            </Grid.Background>

            <StackPanel Orientation="Horizontal" HorizontalAlignment="Center">
                <Button Margin="0,5,0,5" Name="EditItemBtn" Width="115" IsEnabled="False" ToolTip="Bring the selected job to the bottom panel for modification." Click="EditItemBtn_Click" Foreground="#FFE3E3E3">
                    <StackPanel Orientation="Horizontal">
                        <Image Source="icons/full-color/64px/arrow-down.png" Width="24" Height="24"/>
                        <TextBlock Padding="5,0,0,0" VerticalAlignment="Center">Edit Job</TextBlock>
                    </StackPanel>
                </Button>
                <Button Margin="5,5,0,5" Name="NewItemBtn" Width="115" ToolTip="Allow editing of a new JOb.  Note: A job isn't written to the database until it is saved." Click="NewItemBtn_Click">
                    <StackPanel Orientation="Horizontal">
                        <Image Source="icons/full-color/64px/compose.png" Width="24" Height="24"/>
                        <TextBlock Padding="5,0,0,0" VerticalAlignment="Center">New Job</TextBlock>
                    </StackPanel>
                </Button>
                <Button Margin="5,5,0,5" Name="DeleteItemBtn" Width="115" IsEnabled="False" ToolTip="Attempt to delete the current Job." Click="DeleteItemBtn_Click">
                    <StackPanel Orientation="Horizontal">
                        <Image Source="icons/full-color/64px/x.png" Width="24" Height="24"/>
                        <TextBlock Padding="5,0,0,0" VerticalAlignment="Center">Delete Job</TextBlock>
                    </StackPanel>
                </Button>
                <Button Margin="5,5,0,5" Name="SaveItemBtn" Width="115" IsEnabled="False" ToolTip="Write the changes to the database and update the Job list." Click="SaveItemBtn_Click">
                    <StackPanel Orientation="Horizontal">
                        <Image Source="icons/full-color/64px/arrow-up.png" Width="24" Height="24"/>
                        <TextBlock Padding="5,0,0,0" VerticalAlignment="Center">Save Job</TextBlock>
                    </StackPanel>
                </Button>
            </StackPanel>
        </Grid>

        <!-- Edit Item Box -->
        <Grid Grid.Row="4" Width="660">
            <StackPanel Orientation="Horizontal" VerticalAlignment="Center">
                <TextBlock Margin="10,4">Job Name:</TextBlock>
                <TextBox Name="ShiftNameBox" Margin="20,0" Height="22" Width="200" TextChanged="ShiftNameBox_TextChanged" />
                <TextBlock Margin="10,4">Employees Assigned:</TextBlock>
                <TextBlock Margin="10,4" Name="EmployeesAssignedLabel">0</TextBlock>
                <CheckBox Margin="10,5" Name="ItemChanged" Content="Changes not yet saved" IsHitTestVisible="False" Focusable="False"/>
            </StackPanel>
        </Grid>

    </Grid>
</Page>
