﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for EditShiftsPage.xaml
    /// </summary>
    public partial class EditShiftsPage : Page
    {
        List<Shift> Shifts;
        List<Shift> filteredShifts;

        Shift CurrentShift;
        public EditShiftsPage()
        {
            InitializeComponent();

            // Fill the Shifts array
            Shifts = Database.GetAllShiftsForEditing();
            FilterBox_TextChanged(null, null);
            NewItemBtn_Click(null, null);
        }

        private void FilterBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (!String.IsNullOrWhiteSpace(FilterBox.Text))
            {
                var filter = from s in Shifts where s.Name.StartsWith(FilterBox.Text, true, null) select s;
                filteredShifts = new List<Shift>(filter);
            }
            else
                filteredShifts = new List<Shift>(Shifts);

            Grid.ItemsSource = filteredShifts;
        }
        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EditItemBtn.IsEnabled = (Grid.SelectedIndex > -1) ;
        }
        private void ShiftNameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ItemChanged.IsChecked = true;

            if (CurrentShift != null)
                SaveItemBtn.IsEnabled = true;
        }
        private bool PromptToSaveUser()
        {
            // Ask the user if we want to save employee before continuing
            var result = MessageBox.Show("Shift changes have not been saved to the database.\nDo you wish to save them before continuing?", "Warning!  Unsaved Changes!", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Cancel)
                return false;
            if (result == MessageBoxResult.Yes)
                SaveItem(CurrentShift);

            // Both Yes and No return true from here (Because if we return false we are stopping the iniating action from occuring.  Thus-> Cancel returns false).  OTher wise, we only
            // Check for Yes to see if we need to save first before continuing
            return true;
        }

        private void LoadItem(Shift shift)
        {
            // Loads an employee into the bottom / editing portion of the UI
            ShiftNameBox.Text = shift.Name;
            EmployeesAssignedLabel.Text = Database.GetCountOfEmployeesWithShift(shift.ID).ToString();
            ItemChanged.IsChecked = false;

            // Save the employee so we know how to save
            CurrentShift = shift;
        }
        private void SaveItem(Shift shift)
        {
            // Copy the edited information back into the employee instance
            shift.Name = ShiftNameBox.Text;

            // Pass that instance to the database to be inserted / saved
            Database.SaveShift(ref shift);

            // Reset changed bit
            ItemChanged.IsChecked = false;

            // Reloaded Employees for filter
            Shifts = Database.GetAllShiftsForEditing();
            FilterBox_TextChanged(null, null);
        }
        private void EditItemBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will move the selected employee to the lower portion for him to be edited.  
            // If cancel, then return
            if (ItemChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            if (Grid.SelectedIndex == -1)
                return;

            LoadItem((Shift)Grid.SelectedItem);
            SaveItemBtn.IsEnabled = true;
            DeleteItemBtn.IsEnabled = (EmployeesAssignedLabel.Text == "0");
        }
        private void NewItemBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will create a new Shift for the user to edit at the bottom  
            // If they have edited a shift aleady withouit saving, promt to save
            // if cancel, then return
            if (ItemChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            // Create a new shift and load it
            CurrentShift = new Shift(0, String.Empty);
            LoadItem(CurrentShift);
            SaveItemBtn.IsEnabled = false;
            DeleteItemBtn.IsEnabled = false;
        }
        private void SaveItemBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveItem(CurrentShift);
            NewItemBtn_Click(null, null);
        }
        private void DeleteItemBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentShift.ID < 1)
            {
                MessageBox.Show("Please edit a shift before trying to delete it!");
                return;
            }
            Database.DeleteShift(ref CurrentShift);

            // Relaod the shifts
            Shifts = Database.GetAllShiftsForEditing();
            FilterBox_TextChanged(null, null);

            ItemChanged.IsChecked = false;
            DeleteItemBtn.IsEnabled = false;
            NewItemBtn_Click(null, null);
        }
    }
}
