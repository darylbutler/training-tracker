﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingLogBySOPReportWindow.xaml
    /// </summary>
    public partial class TrainingLogBySOPReportWindow : Window
    {
        List<TrainingLog> Logs;

        public TrainingLogBySOPReportWindow(List<TrainingLog> log)
        {
            InitializeComponent();

            Logs = log;

            sopName.Text = log.First().SOP.Name;
            sopOwner.Text = log.First().SOP.Owner;
            sopVersion.Text = log.First().SOP.Version;

            GenerateRows();
            PrintReport();
        }
        private void GenerateRows()
        {
            // Generate Report Table from logs

            // Generate one row per training log
            TableRowGroup rowGroup = new TableRowGroup();
            bool altRow = false;
            foreach (var log in Logs)
            {
                TableRow row = new TableRow();
                if (altRow)
                    row.Background = Brushes.LightGray;

                // Training Date
                Paragraph p = new Paragraph(new Run(log.TrainingDate.GetValueOrDefault().ToString("yyyy-MM-dd")));
                p.TextAlignment = TextAlignment.Center;
                var cell = new TableCell(p);
                cell.ColumnSpan = 2;
                row.Cells.Add(cell);

                // EE lName
                p = new Paragraph(new Run(log.Employee.LastName));
                p.TextAlignment = TextAlignment.Left;
                cell = new TableCell(p);
                cell.ColumnSpan = 2;
                row.Cells.Add(cell);

                // EE fName
                p = new Paragraph(new Run(log.Employee.FirstName));
                p.TextAlignment = TextAlignment.Left;
                cell = new TableCell(p);
                cell.ColumnSpan = 2;
                row.Cells.Add(cell);

                // EE Job
                p = new Paragraph(new Run(log.Employee.Job));
                p.TextAlignment = TextAlignment.Center;
                cell = new TableCell(p);
                cell.ColumnSpan = 2;
                row.Cells.Add(cell);

                // EE Shift
                p = new Paragraph(new Run(log.Employee.Shift));
                p.TextAlignment = TextAlignment.Center;
                cell = new TableCell(p);
                cell.ColumnSpan = 2;
                row.Cells.Add(cell);

                rowGroup.Rows.Add(row);
                altRow = !altRow;
            }

            Report.RowGroups.Add(rowGroup);
        }

        public void PrintReport()
        {
            // Create a XpsDocumentWriter object, implicitly opening a Windows common print dialog,
            // and allowing the user to select a printer.

            // get information about the dimensions of the seleted printer+media.
            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)flowDoc).DocumentPaginator;

                // Change the PageSize and PagePadding for the document to match the CanvasSize for the printer device.
                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);;

                Thickness t = new Thickness(72);  // copy.PagePadding;
                flowDoc.PagePadding = new Thickness(
                                   Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                flowDoc.ColumnWidth = double.PositiveInfinity;

                // Change size of window to paper size before window is shown
                ReportWindow.Width = paginator.PageSize.Width;
                ReportWindow.Height = paginator.PageSize.Height;

                TitleLogo.Width = Math.Min((ReportWindow.Width - 540 - (flowDoc.PagePadding.Left + flowDoc.PagePadding.Right)), 300);

                // Send content to the printer.
                docWriter.Write(paginator);                
            }
        }
    }
}
