﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KelseyTrainingApp
{
    public class TrainingLog
    {
        private long id;
        public long ID { get { return id; } }
        public Employee Employee { get; set; }
        public SOP SOP { get; set; }
        public DateTime? TrainingDate { get; set; }
        public String IsDueString {  get
            {
                if (TrainingDate == null || DateTime.Now > TrainingDate)
                    return "Overdue";

                return DateTime.Now.Subtract(TrainingDate ?? new DateTime()).Days.ToString() + " Days";
            } }
        
        public TrainingLog(int _id)
        {
            id = _id;
        }
        public TrainingLog()
        {
            id = -1;
        }
        public void UpdateID(int _id)
        {
            id = _id;
        }
    }
}
