﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingMatrixPage.xaml
    /// </summary>
    public partial class TrainingMatrixPage : Page
    {
        ObservableCollection<Record> Matrix;
        List<string> Shifts;
        List<string> Jobs;
        DateToColorConverter BGConverter;
        DateToColorConverter FGConverter;

        public TrainingMatrixPage()
        {
            InitializeComponent();
            
            Shifts = new List<string>();
            Jobs = new List<string>();
            Shifts.Add("<All>");
            Shifts.AddRange(Database.GetAllShifts());
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;
            Jobs.Add("<All>");
            Jobs.AddRange(Database.GetAllJobs());
            FilterEEByJobBox.ItemsSource = Jobs;
            FilterEEByJobBox.SelectedIndex = 0;

        }

        private void GenerateColumns()
        {
            BGConverter = new DateToColorConverter();
            BGConverter.OverdueStr = "Overdue";
            BGConverter.NAStr = "N/A";
            BGConverter.NABrush = Brushes.LightGray;
            BGConverter.OverDueBrush = Brushes.PaleVioletRed;            
            BGConverter.DefaultBrush = null;

            FGConverter = new DateToColorConverter();
            FGConverter.OverdueStr = "Overdue";
            FGConverter.NAStr = "N/A";
            FGConverter.NABrush = Brushes.DarkGray;
            FGConverter.OverDueBrush = Brushes.White;
            FGConverter.DefaultBrush = Brushes.Black;

            ReportGrid.Columns.Clear();

            // FIX: 20160726 DJB -- TrainingMatrix crashed when filtering for employees that didn't exist (Employees array is passed in empty)
            // Return if Matrix is empty
            if (Matrix.Count < 1) return;

            var columns = Matrix.First()
                          .Properties
                          .Select((x, i) => new { Name = x.Name, Index = i })
                          .ToArray();

            foreach (var column in columns)
            {
                var binding = new Binding(string.Format("Properties[{0}].Value", column.Index));

                var col = new DataGridTextColumn() { Header = column.Name, Binding = binding };
                col.HeaderStyle = (Style) FindResource("DataGrid_ColumnHeaderRotateStyle");

                if (column.Index < 5)
                {
                    // First 5 columns are Employee data and are dark grey
                    Style s = new Style(typeof(DataGridCell));
                    SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(74, 157, 189, 198));
                    s.Setters.Add(new Setter(DataGridCell.BackgroundProperty, brush));
                    s.Setters.Add(new Setter(DataGridCell.ForegroundProperty, Brushes.Black));                   
                    col.CellStyle = s;
                    col.MinWidth = 50;

                    // Hide the 'Active' column is we're already filtering out inactive employees
                    if (column.Index == 2 && (FilterEEByActiveBox.IsChecked ?? false))
                        col.Visibility = Visibility.Collapsed;
                }
                else
                {
                    // Highlight values of SOP columns that are overdue or Not Applicable

                    Style s = new Style(typeof(DataGridCell));

                    Setter bgSetter = new Setter();
                    bgSetter.Property = DataGridCell.BackgroundProperty;
                    var bgBindng = new Binding(string.Format("Properties[{0}].Value", column.Index));
                    bgBindng.Converter = BGConverter;
                    bgSetter.Value = bgBindng;
                    s.Setters.Add(bgSetter);

                    Setter fgSetter = new Setter();
                    fgSetter.Property = DataGridCell.ForegroundProperty;
                    var fgBindng = new Binding(string.Format("Properties[{0}].Value", column.Index));
                    fgBindng.Converter = FGConverter;
                    fgSetter.Value = fgBindng;
                    s.Setters.Add(fgSetter);

                    col.CellStyle = s;
                    col.Width = 130;       
                    
                }
                // Cell Content Alignment
                Style sty = new Style(typeof(TextBlock));
                sty.Setters.Add(new Setter(TextBlock.TextAlignmentProperty, TextAlignment.Center));
                sty.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
                col.ElementStyle = sty;

                ReportGrid.Columns.Add(col);
            }
            ReportGrid.RowHeight = 35;
        }
        private async void ApplyFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            // Show the busy window...
            BusyWindow win = new BusyWindow();
            win.Owner = App.Current.MainWindow;
            win.Show();

            var employees = await Task.Run(async () =>
            {
                var allEmp = await Task.Run(() => { return Database.GetAllEmployees(); });
                return from em in allEmp select em;
            });

            // Filter the employees
            if (!String.IsNullOrWhiteSpace(FilterEEByFNameBox.Text))
                employees = from d in employees where d.FirstName.StartsWith(FilterEEByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterEEByLNameBox.Text))
                employees = from d in employees where d.LastName.StartsWith(FilterEEByLNameBox.Text, true, null) select d;
            if (FilterEEByShiftBox.SelectedIndex > 0)
                employees = from d in employees where d.Shift == FilterEEByShiftBox.SelectedItem.ToString() select d;
            if (FilterEEByJobBox.SelectedIndex > 0)
                employees = from d in employees where d.Job.StartsWith(FilterEEByJobBox.SelectedItem.ToString(), true, null) select d;
            if (FilterEEByActiveBox.IsChecked ?? false)
                employees = from d in employees where d.Active == true select d;

            var filteredEmployees = employees.ToList<Employee>();

            // FIX: 20160726 DJB -- TrainingMatrix crashed when filtering for employees that didn't exist (Employees array is passed in empty)
            // Alert and Return if filteredEmployees is Empty
            if (filteredEmployees.Count < 1)
            {
                
                MessageBox.Show("No Employees Found", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                var result = await Task.Run(async () =>
                {
                    var x = await Task.Run(() => { return Database.GetTrainingMatrix(filteredEmployees); });
                    return x;
                });

                Matrix = new ObservableCollection<Record>(result);
                GenerateColumns();
                ReportGrid.ItemsSource = Matrix;
            }

            // Close the Busy Window
            App.Current.MainWindow.Activate();
            win.Close();
        }
        private void ClearFiltersBtn_Click(object sender, RoutedEventArgs e)
        {
            // Clear all the filter input boxes and call the filter function
            FilterEEByFNameBox.Text = String.Empty;
            FilterEEByLNameBox.Text = String.Empty;
            FilterEEByActiveBox.IsChecked = true;
            FilterEEByShiftBox.SelectedIndex = 0;
            FilterEEByJobBox.SelectedIndex = 0;
            ApplyFilterBtn_Click(null, null);
        }

        // -- Show / Hide the filter via toolbar button (For fullscreen view)
        private void FilterShowBtn_Checked(object sender, RoutedEventArgs e)
        {
            FilterGroupBox.Visibility = Visibility.Visible;
        }
        private void FilterShowBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterGroupBox.Visibility = Visibility.Collapsed;
        }

        // Show this page in fullscreen view
        private void FullscreenBtn_Click(object sender, RoutedEventArgs e)
        {
            // Hide the Fullscreen button so we cannot fullscreen from the fullscreen screen
            FullscreenBtn.Visibility = Visibility.Collapsed;

            // Launch the fullscreen window with this Page
            FullscreenWindow win = new FullscreenWindow(this);

            // Show the window
            win.Show();
        }
        // Export matrix to CSV
        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Matrix == null || Matrix.Count < 1) return;

            System.Windows.Forms.SaveFileDialog Savedlg = new System.Windows.Forms.SaveFileDialog();
            Savedlg.Filter = "CSV Files (*.csv)|*.csv|All Files|*.*";
            Savedlg.Title = "Export to CSV file...";

            if (Savedlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string filename = Savedlg.FileName;
            List<string> file = new List<string>(Matrix.Count + 1);

            // Create the CSV

            // Header line
            bool HeaderGenerated = false;

            foreach (var r in Matrix)
            {
                if (!HeaderGenerated)
                {
                    // Select all Property Titles
                    var q = from p in r.Properties select quote(p.Name);
                    file.Add(String.Join(",", q));
                    HeaderGenerated = true;
                }

                // Add all the values, joined with commas
                // Bug 1 -> Added .ToString().Replace("\n", " -- ") because the new lines used for display messed up the resulting csv output
                var query = from p in r.Properties select quote(p.Value.ToString().Replace("\n", " -- "));

                file.Add(String.Join(",", query));
            }

            // Write this out to the selected file
            System.IO.File.WriteAllLines(filename, file);
        }
        private string quote(string s)
        {
            return '"' + s.Replace("\"", "\"\"") + '"';
        }
    }
    public class DateToColorConverter : IValueConverter
    {
        public Brush OverDueBrush { get; set; }
        public Brush NABrush { get; set; }
        public Brush DefaultBrush { get; set; }

        public string OverdueStr { get; set; }
        public string NAStr { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return DefaultBrush;

            string val = value.ToString();

            if (val.Contains(NAStr))
                return NABrush;
            if (val.Contains(OverdueStr))
                return OverDueBrush;
            else
                return DefaultBrush;

        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
