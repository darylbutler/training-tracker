﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KelseyTrainingApp
{
    public class Shift
    {
        // This class exists only for editing the Shifts in the database.  Everywhere else in the program,
        // handle shifts as plain old strings
        long id;
        public long ID { get { return id; } }
        public string Name { get; set; }

        public Shift(long _ID, string _Name)
        {
            this.id = _ID;
            this.Name = _Name;
        }
        public void UpdateID(long _id)
        {
            this.id = _id;
        }
    }
}
