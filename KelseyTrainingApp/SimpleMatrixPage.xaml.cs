﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for SimpleMatrixPage.xaml
    /// </summary>
    public partial class SimpleMatrixPage : Page
    {
        List<Record> Matrix;
        List<string> Shifts;
        List<string> Jobs;

        public SimpleMatrixPage()
        {
            InitializeComponent();

            // Load Data
            Shifts = new List<string>();
            Jobs = new List<string>();
            Shifts.Add("<All>");
            Shifts.AddRange(Database.GetAllShifts());
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;
            Jobs.Add("<All>");
            Jobs.AddRange(Database.GetAllJobs());
            FilterEEByJobBox.ItemsSource = Jobs;
            FilterEEByJobBox.SelectedIndex = 0;
        }

        private async void ApplyFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            // Show the busy window...
            BusyWindow win = new BusyWindow();
            win.Owner = App.Current.MainWindow;
            win.Show();

            var employees = from em in Database.GetAllEmployees() select em;

            // Filter the employees
            if (!String.IsNullOrWhiteSpace(FilterEEByFNameBox.Text))
                employees = from d in employees where d.FirstName.StartsWith(FilterEEByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterEEByLNameBox.Text))
                employees = from d in employees where d.LastName.StartsWith(FilterEEByLNameBox.Text, true, null) select d;
            if (FilterEEByShiftBox.SelectedIndex > 0)
                employees = from d in employees where d.Shift == FilterEEByShiftBox.SelectedItem.ToString() select d;
            if (FilterEEByJobBox.SelectedIndex > 0)
                employees = from d in employees where d.Job.StartsWith(FilterEEByJobBox.SelectedItem.ToString(), true, null) select d;
            if (FilterEEByActiveBox.IsChecked ?? false)
                employees = from d in employees where d.Active == true select d;

            var filteredEmployees = employees.ToList<Employee>();

            // FIX: 20160726 DJB -- TrainingMatrix crashed when filtering for employees that didn't exist (Employees array is passed in empty)
            // Alert and Return if filteredEmployees is Empty
            if (filteredEmployees.Count < 1)
            {
                MessageBox.Show("No Employees Found", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                // Get the training matrix matching these employees
                var result = await Task.Run(async () =>
                {
                    var x = await Task.Run(() => { return Database.GetTrainingMatrix(filteredEmployees); });
                    return x;
                });

                Matrix = new List<Record>(result);
                GenerateColumns();
                ReportGrid.ItemsSource = Matrix;

                // Sort the Grid manually
                SortDataGrid(ReportGrid, 3);
            }

            // Close the Busy Window
            App.Current.MainWindow.Activate();
            win.Close();
        }
        private void ClearFiltersBtn_Click(object sender, RoutedEventArgs e)
        {
            // Clear all the filter input boxes and call the filter function
            FilterEEByFNameBox.Text = String.Empty;
            FilterEEByLNameBox.Text = String.Empty;
            FilterEEByActiveBox.IsChecked = true;
            FilterEEByShiftBox.SelectedIndex = 0;
            FilterEEByJobBox.SelectedIndex = 0;
            ApplyFilterBtn_Click(null, null);
        }
        public static void SortDataGrid(DataGrid dataGrid, int columnIndex = 0, ListSortDirection sortDirection = ListSortDirection.Ascending)
        {
            var column = dataGrid.Columns[columnIndex];

            // Clear current sort descriptions
            dataGrid.Items.SortDescriptions.Clear();

            // Add the new sort description
            dataGrid.Items.SortDescriptions.Add(new SortDescription(column.SortMemberPath, sortDirection));

            // Apply sort
            foreach (var col in dataGrid.Columns)
            {
                col.SortDirection = null;
            }
            column.SortDirection = sortDirection;

            // Refresh items to display sort
            dataGrid.Items.Refresh();
        }

        // Layout the data grid
        private void GenerateColumns()
        {
            ReportGrid.Columns.Clear();
            ReportGrid.RowHeight = 64;

            // FIX: 20160726 DJB -- TrainingMatrix crashed when filtering for employees that didn't exist (Employees array is passed in empty)
            // Return if Matrix is empty
            if (Matrix.Count < 1) return;

            var columns = Matrix.First()
                          .Properties
                          .Select((x, i) => new { Name = x.Name, Index = i })
                          .ToArray();

            foreach (var column in columns)
            {
                var binding = new Binding(string.Format("Properties[{0}].Value", column.Index));
                DataGridColumn col = new DataGridTextColumn() { Header = column.Name, Binding = binding };

                //col.HeaderStyle = (Style)FindResource("DataGrid_ColumnHeaderRotateStyle");

                if (column.Index < 5)
                {
                    // First 5 columns are Employee data and are dark grey
                    Style s = new Style(typeof(DataGridCell));
                    SolidColorBrush brush = new SolidColorBrush(Color.FromArgb(74, 157, 189, 198));
                    s.Setters.Add(new Setter(DataGridCell.BackgroundProperty, brush));
                    s.Setters.Add(new Setter(DataGridCell.ForegroundProperty, Brushes.Black));
                    col.CellStyle = s;
                    col.MinWidth = 50;

                    if (column.Index == 2 && (FilterEEByActiveBox.IsChecked ?? false))
                        col.Visibility = Visibility.Collapsed;
                }
                else
                {
                    // Create a Column Template for Image Display
                    DataGridTemplateColumn col1 = new DataGridTemplateColumn();
                    col1.Header = column.Name;
                    FrameworkElementFactory factory1 = new FrameworkElementFactory(typeof(Image));
                    binding.Converter = new ValueToImageConverter();                    
                    factory1.SetValue(Image.SourceProperty, binding);
                    factory1.SetValue(Image.WidthProperty, 50d);
                    factory1.SetValue(Image.HeightProperty, 50d);
                    DataTemplate cellTemplate1 = new DataTemplate();
                    cellTemplate1.VisualTree = factory1;
                    col1.CellTemplate = cellTemplate1;

                    col = col1;
                    col.Width = 64;
                }
                col.HeaderStyle = (Style)FindResource("DataGrid_ColumnHeaderRotateStyle");
                // Cell Content Alignment
                Style sty = new Style(typeof(TextBlock));
                sty.Setters.Add(new Setter(TextBlock.TextAlignmentProperty, TextAlignment.Center));
                sty.Setters.Add(new Setter(TextBlock.VerticalAlignmentProperty, VerticalAlignment.Center));
                //col.ElementStyle = sty;

                ReportGrid.Columns.Add(col);
            }
        }

        // -- Show / Hide the filter via toolbar button (For fullscreen view)
        private void FilterShowBtn_Checked(object sender, RoutedEventArgs e)
        {
            FilterGroupBox.Visibility = Visibility.Visible;
        }
        private void FilterShowBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterGroupBox.Visibility = Visibility.Collapsed;
        }

        // Show this page in fullscreen view
        private void FullscreenBtn_Click(object sender, RoutedEventArgs e)
        {
            // Hide the Fullscreen button so we cannot fullscreen from the fullscreen screen
            FullscreenBtn.Visibility = Visibility.Collapsed;

            // Launch the fullscreen window with this Page
            FullscreenWindow win = new FullscreenWindow(this);

            // Show the window
            win.Show();
        }
    }

    public class ValueToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !(value is string))
                return new BitmapImage(new Uri("icons/full-color/64px/x.png", UriKind.Relative));

            string val = value.ToString();

            if (val.Contains("N/A"))
                return null;
            if (val.Contains("Overdue"))
                return new BitmapImage(new Uri("icons/full-color/64px/x.png", UriKind.Relative));

            return new BitmapImage(new Uri("icons/full-color/64px/check.png", UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
