﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for BackupDatabasePage.xaml
    /// </summary>
    public partial class BackupDatabasePage : Page
    {
        public BackupDatabasePage()
        {
            InitializeComponent();

            // Load last used paths from settings
            OnlineBackupPath.Text = Properties.Settings.Default.lastOnlineBackupPath;
            //FileBackupPath.Text = Properties.Settings.Default.lastFileBackupPath;

            // Choose best backup path for the restore path box
            if (Properties.Settings.Default.lastFileBackupDateTime > Properties.Settings.Default.lastOnlineBackupDateTime)
                RestorePath.Text = Properties.Settings.Default.lastFileBackupPath + "\\training_database_file_backup.db";
            else
                RestorePath.Text = Properties.Settings.Default.lastOnlineBackupPath + "\\training_database_online_backup.db";
        }

        //private void FileBackupBrowseBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
        //    dlg.Description = "Select the folder to save the database backup file to.";
        //    dlg.ShowNewFolderButton = true;

        //    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //        FileBackupPath.Text = dlg.SelectedPath;
        //}

        //private void FileBackupStartBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    // Perform the database backup by copying database from our folder to the selected path

        //    // Return if no path selected
        //    if (String.IsNullOrWhiteSpace(FileBackupPath.Text) || FileBackupPath.Text == "File Backup Success!")
        //    {
        //        MessageBox.Show("Please select a path before attempting the File Backup.");
        //        return;
        //    }

        //    // Copy the file, delete if file exists at location
        //    string backupPath = FileBackupPath.Text + "\\training_database_file_backup.db";

        //    // Make sure the file isn't the same as the application database!
        //    if (backupPath == Database.DatabasePath)
        //    {
        //        MessageBox.Show("In FileBackup Start, CANNOT BACK UP OVER THE PRIMARY DATABASE!  Choose a better [wiser] path to back up to, one that ISN'T IN THE SAME APPLICATION DIRECTORY!, and try again.", "Backup Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        //        return;
        //    }

        //    if (File.Exists(backupPath))
        //        File.Delete(backupPath);

        //    string dbPath = Database.DatabasePath;

        //    File.Copy(dbPath, backupPath);

        //    // If we were successful, indicate success, and save the path
        //    Properties.Settings.Default.lastFileBackupPath = FileBackupPath.Text;
        //    Properties.Settings.Default.lastFileBackupDateTime = DateTime.Now;
        //    FileBackupPath.Text = "File Backup Success!";
        //    FileBackupStartBtn.IsEnabled = false;            
        //}

        private void OnlineBackupBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            // Dialog to browse to backup path
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.Description = "Select the folder to backup the database to:";
            dlg.ShowNewFolderButton = true;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                OnlineBackupPath.Text = dlg.SelectedPath;
        }
        private async void OnlineBackupStartBtn_Click(object sender, RoutedEventArgs e)
        {
            BusyWindow win = null;


            // Save the Database if Modified
            if (Database.Modified)
            {
                var result = MessageBox.Show("Do you want to save the database back to the disk?\nIf you press No, any changes made in this session will be lost.", "Unsaved Changes!", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
                App.Current.MainWindow.Activate();
                if (result == MessageBoxResult.Cancel)
                    return;
                else if (result == MessageBoxResult.Yes)
                {
                    // Show busy window while saving the database                    
                    win = new BusyWindow();
                    win.WorkingLabel.Text = "Saving Database...";
                    win.Owner = App.Current.MainWindow;
                    win.Show();

                    await Task.Run(async () =>
                    {
                        await Task.Run(() => { Database.SaveDatabase(); });
                    });
                }
            }

            // Show busy dialog if not already shown
            if (win == null)
            {
                win = new BusyWindow();
                win.Owner = App.Current.MainWindow;
                win.Show();
            }
            win.WorkingLabel.Text = "Backing up Database...";

            // Establish a connection to the new database
            string backupDB = OnlineBackupPath.Text + "\\training_database_online_backup.db";

            // Make sure the file isn't the same as the application database!
            if (backupDB == Properties.Settings.Default.RemoteDatabasePath)
            {
                MessageBox.Show("In OnlineBackup Start, CANNOT BACK UP OVER THE PRIMARY DATABASE!  Choose a better [wiser] path to back up to, one that ISN'T IN THE SAME APPLICATION DIRECTORY!, and try again.", "Backup Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Perform the Online Backup
            await Task.Run(async () =>
            {
                await Task.Run(() => { Database.BackupDatabase(backupDB); });
            });

            // If we were successful, indicate success and save path
            Properties.Settings.Default.lastOnlineBackupPath = OnlineBackupPath.Text;
            Properties.Settings.Default.lastOnlineBackupDateTime = DateTime.Now;
            OnlineBackupPath.Text = "Online Backup Success!";
            OnlineBackupStartBtn.IsEnabled = false;

            // Close the busy dialog
            win.Close();
        }
        private void RestoreBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.Filter = "Training Log Databases|*.db";
            dlg.InitialDirectory = RestorePath.Text;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                RestorePath.Text = dlg.FileName;
        }        
        private async void RestoreStartBtn_Click(object sender, RoutedEventArgs e)
        {
            // Make sure an existing file is selected
            if (!File.Exists(RestorePath.Text))
            {
                MessageBox.Show("Specified database file does not exist or it cannot be found!", "Cannot restore this database", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Check if the database is a valid SQLite3 database
            if (!Database.DatabaseIsValid(RestorePath.Text))
            {
                MessageBox.Show("Specified database file isn't a databse file!", "Cannot restore this database", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Check if the database is a database we created (Uses our 6 tables)
            if (!Database.DatabaseMatchesOurSchema(RestorePath.Text))
            {
                MessageBox.Show("Specified database file isn't a valid Training Tracker databse file!\nIt could be corrupted, but it doesn't match our database pattern", "Cannot restore this database", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Show the busy Dialog
            BusyWindow win = new BusyWindow();
            win.WorkingLabel.Text = "Restoring Database...";
            win.Owner = App.Current.MainWindow;
            win.Show();

            // Perform the restore            
            await Task.Run(async () =>
            {
                await Task.Run(() => { Database.RestoreDatabase(RestorePath.Text); });
            });

            // Data is good now, close the busy window
            win.Close();
        }
    }
}
