﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace KelseyTrainingApp
{
	static class Database
	{
		// Path to DB
		public static string DatabasePath = Properties.Settings.Default.RemoteDatabasePath;
		public static bool Loaded = false;  // True once we've successfully loaded a database into memory.  Set to false to invalidate a database

		public static SQLiteConnection InMemoryDatabase;
		private static bool _modified;
		public static bool Modified { get { return _modified; } set { _modified = value; } }

		private static SQLiteConnection Connect()
		{
			if (InMemoryDatabase == null || InMemoryDatabase.State != System.Data.ConnectionState.Open || !Loaded)
			{
				// Release database in memory if it has been invalided with Loaded
				if (!Loaded && (InMemoryDatabase != null))
					InMemoryDatabase.Dispose();

				// Ensure we're opening the latest DB path
				DatabasePath = Properties.Settings.Default.RemoteDatabasePath;

				// Start Open off new Database file
				InMemoryDatabase = new SQLiteConnection("Data Source=:memory:");
				InMemoryDatabase.Open();

				// Load the database
				SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
				conn.DataSource = DatabasePath;
				conn.Version = 3;
				using (var fileDB = new SQLiteConnection(conn.ConnectionString, true))
				{
					fileDB.Open();
					fileDB.BackupDatabase(InMemoryDatabase, "main", "main", -1, null, 0);
					Modified = false;
					Loaded = true;
				}
			}

			return InMemoryDatabase;
		}
		public static void PreloadDatabase()
		{
			Loaded = false;	// Invalidate the current database
			Connect();
		}
		public static void SaveDatabase()
		{
			if (Loaded && InMemoryDatabase != null && InMemoryDatabase.State == System.Data.ConnectionState.Open)
			{
				// Load the database
				SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
				conn.DataSource = DatabasePath;
				conn.Version = 3;
				using (var fileDB = new SQLiteConnection(conn.ConnectionString, true))
				{
					fileDB.Open();
					InMemoryDatabase.BackupDatabase(fileDB, "main", "main", -1, null, 0);
					Modified = false;
				}
			}
		}
		public static void CreateNewDatabase(string newDatabasePath)
		{
			// Create a brand new database file		
			if (File.Exists(newDatabasePath))
				File.Delete(newDatabasePath);

			// Path to the new database
			string path = new Uri(newDatabasePath).LocalPath;
			SQLiteConnection.CreateFile(path);

			// Actually create the new database
			SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
			conn.DataSource = path;
			conn.Version = 3;
			using (SQLiteConnection connection = new SQLiteConnection(conn.ConnectionString, true))
			{
				if (connection.State != System.Data.ConnectionState.Open) connection.Open();
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{
					// Create the tables
					cmd.CommandText = @"CREATE TABLE shifts       (id INTEGER PRIMARY KEY, name TEXT);
										CREATE TABLE jobs         (id INTEGER PRIMARY KEY, name TEXT);
										CREATE TABLE employees    (id INTEGER PRIMARY KEY, fname TEXT, lname TEXT, shift_id INT, job_id INT, active INT);
										CREATE TABLE sops         (id INTEGER PRIMARY KEY, name TEXT, version TEXT, owner TEXT, active INT, renewal INT);
										CREATE TABLE sop_to_job   (id INTEGER PRIMARY KEY, sop_id INT, job_id INT);
										CREATE TABLE training_log (id INTEGER PRIMARY KEY AUTOINCREMENT, tdate TEXT, sop_id INT, employee_id INT);
										CREATE TABLE roles        (id INTEGER PRIMARY KEY, employee_id INT, job_id INT);";
					cmd.ExecuteNonQuery();
				}
			}

			/*
			// We don't know where we are in the Application process when we are recreating the db, 
			// so the easiest route is to just restart the app because it will automatically load 
			// the empty db on startup
			DatabasePath = newDatabasePath;
			Properties.Settings.Default.RemoteDatabasePath = newDatabasePath;   // Switch to the new Database
			Properties.Settings.Default.Save();									// Save any changes
			RestartApplication();												// Restart to load it
			*/
		}
		public static void BackupDatabase(string backupDBPath)
		{
			// Use the Sqlite3 Online Backup method to backup database to specified location
			if (Modified)
				SaveDatabase();

			// TODO should be invalidate the data here to force a reload from the remote address?  
			// This would ensure a direct backup, but I think typical use case is just 
			// OpenApp (Which Loads database) -> Backup
			// So there's little need to force a reload
			using (SQLiteConnection connection = Connect())
			{
				SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
				conn.DataSource = backupDBPath;
				conn.Version = 3;
				using (var backupDBConnection = new SQLiteConnection(conn.ConnectionString))
				{
					backupDBConnection.Open();

					// Perform the backup
					connection.BackupDatabase(backupDBConnection, "main", "main", -1, null, 0);                    
				}
			}
		}
		public static void RestoreDatabase(string dbPath)
		{
			// Invalidate loaded database, and load from the supplied database
			Loaded = false;
			string path = Properties.Settings.Default.RemoteDatabasePath;   // Save the correct database path
			Properties.Settings.Default.RemoteDatabasePath = dbPath;        // We're loading this external database to memory

			// Load the external database
			using (SQLiteConnection connection = Connect())
			{
				SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
				conn.DataSource = path; // Save to the current remote db path
				conn.Version = 3;
				using (var restoreDBConnection = new SQLiteConnection(conn.ConnectionString))
				{
					restoreDBConnection.Open();

					// Perform the backup
					connection.BackupDatabase(restoreDBConnection, "main", "main", -1, null, 0);
				}
			}

			// Now, reload the data a final time to ensure everything is a-OKAY
			Properties.Settings.Default.RemoteDatabasePath = path;  // Restore this to the correct value
			InMemoryDatabase.Dispose();
			InMemoryDatabase = null;
			PreloadDatabase();  // This is will the database
		}        
		public static void RestartApplication()
		{
			System.Diagnostics.Process.Start(System.Windows.Application.ResourceAssembly.Location);
			System.Windows.Application.Current.Shutdown();
		}

		#region -- Database Get Helpers --
		// -- Shifts ------------------------------------------------------------------------------
		public static List<string> GetAllShifts()
		{
			List<string> ret = new List<string>();
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT name FROM shifts ORDER BY name DESC;";
				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						ret.Add(reader["name"].ToString());                        
				}                    
			}            

			return ret;
		}
		public static List<Shift> GetAllShiftsForEditing()
		{
			List<Shift> ret = new List<Shift>();
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{

				cmd.CommandText = "SELECT * FROM shifts ORDER BY id DESC;";

				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						ret.Add(new Shift(Int32.Parse(reader["id"].ToString()), reader["name"].ToString()));
				}
			}
			
			return ret;
		}
		// -- Jobs --------------------------------------------------------------------------------
		public static List<string> GetAllJobs()
		{
			List<string> ret = new List<string>();
			SQLiteConnection connection = Connect();            

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT name FROM jobs ORDER BY name DESC;";
				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						ret.Add(reader["name"].ToString());
				}
			}

			return ret;
		}
		public static List<Job> GetAllJobsForEditing()
		{
			List<Job> ret = new List<Job>();
			SQLiteConnection connection = Connect();            

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT * FROM jobs ORDER BY id DESC;";

				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						ret.Add(new Job(Int32.Parse(reader["id"].ToString()), reader["name"].ToString()));
				}
			}
			
			return ret;
		}
		// -- Employees ---------------------------------------------------------------------------
		public static List<Employee> GetAllEmployees()
		{
			List<Employee> ret = new List<Employee>();
			SQLiteConnection connection = Connect();            

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Query for employees
				cmd.CommandText = @"SELECT employees.id, employees.fname, employees.lname, shifts.name AS shift, jobs.name AS job, employees.active, employees.job_id, employees.shift_id 
									FROM employees
									JOIN shifts ON employees.shift_id = shifts.id
									JOIN jobs ON employees.job_id = jobs.id
									ORDER BY employees.lname ASC;";

				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						var em = new Employee(reader["fname"].ToString(),
												reader["lname"].ToString(),
												reader["shift"].ToString(),
												reader["job"].ToString(),
												Int32.Parse(reader["active"].ToString()) == 0 ? false : true,
												Int32.Parse(reader["id"].ToString()),
												Int32.Parse(reader["shift_id"].ToString()),
												Int32.Parse(reader["job_id"].ToString())
											);
						GetEmployeeRoles(ref em);
						ret.Add(em);
					}
				}
			}
			
			return ret;
		}
		public static void GetEmployeeRoles(ref Employee ee)
		{
			ee.Roles = new List<string>();
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT jobs.name FROM roles JOIN jobs ON roles.job_id=jobs.id WHERE roles.employee_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", ee.ID);

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						ee.Roles.Add(reader["name"].ToString());
				}
			}        
		}
		public static long GetCountOfEmployeesWithShift(long id)
		{
			// Returns number of employees assigned to a particular shift
			//
			// We use ID here because we want to be absolutely sure with this metric, because it may allow a Shift/Job to be deleted when it shouldn't
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT COUNT(*) FROM employees WHERE shift_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", id);

				return long.Parse(cmd.ExecuteScalar().ToString());
			}            
		}
		public static long GetCountOfEmployeesWithJob(long id)
		{
			// Returns number of employees assigned to a particular Job
			//
			// We use ID here because we want to be absolutely sure with this metric, because it may allow a Shift/Job to be deleted when it shouldn't
			SQLiteConnection connection = Connect();            

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT COUNT(*) FROM employees WHERE job_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", id);

				return long.Parse(cmd.ExecuteScalar().ToString());
			}            
		}
		// -- SOPs --------------------------------------------------------------------------------
		public static List<SOP> GetAllSOPs()
		{
			// Returns all SOPs in the database
			// DOES NOT RETURN LIST OF ALL 'AppliesToJobs' LINKS
			List<SOP> ret = new List<SOP>();
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Query for employees
				cmd.CommandText = @"SELECT id, name, version, owner, active, renewal
									FROM sops
									ORDER BY active ASC, name ASC;";

				using (SQLiteDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						var sop = new SOP()
						{
							Name = reader["name"].ToString(),
							Version = reader["version"].ToString(),
							Owner = reader["owner"].ToString(),
							Active = reader["active"].ToString() == "1"
						};
						string renewal = reader["renewal"].ToString();
						sop.Renewal = String.IsNullOrWhiteSpace(renewal) ? 0 : Int32.Parse(renewal);
						sop.UpdateID(Int32.Parse(reader["id"].ToString()));
						ret.Add(sop);
					}
				}                    
			}
			return ret;
		}
		public static void GetSOPAppliesToJobs(ref SOP sop)
		{
			// Loads all the jobs this SOP applies to (in the 'sop_to_job' table)

			// Clear the list
			sop.AppliesToJobs = new List<string>();

			// if passed sop does not have an ID (It cannot possibly have any jobs),
			// simply return with the empty list
			if (sop.ID == -1)
				return;

			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = @"SELECT jobs.name
									FROM sop_to_job
									JOIN jobs ON sop_to_job.job_id = jobs.id
									WHERE sop_to_job.sop_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", sop.ID);

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
						sop.AppliesToJobs.Add(reader["name"].ToString());
				}
			}
		}
		// -- TrainingLog -------------------------------------------------------------------------
		public static List<TrainingLog> GetTrainingLogByEmployee(ref Employee ee, bool OnlyActiveSOPs, DateTime? startDate, DateTime? endDate)
		{
			/*
				Query:
				SELECT sops.id AS id, sops.name AS name, sops.version AS version, sops.owner AS owner, sops.active AS active, sops.renewal AS renewal, training_log.tdate AS tdate
				FROM training_log
				JOIN sops ON training_log.sop_id=sops.id
				WHERE training_log.employee_id=1;
			*/

			List<TrainingLog> log = new List<TrainingLog>();
			string sql = String.Empty,
				queryStr = String.Format("WHERE training_log.employee_id={0}", ee.ID);



			// If the date range is not null, we're filtering by dates
			if (startDate.HasValue && endDate.HasValue)
			{
				DateTime start = startDate.Value;
				DateTime end = endDate.Value;

				queryStr = String.Format("{0} AND (tdate BETWEEN '{1}' AND '{2}')",
											queryStr,
											start.ToString("yyyy-MM-dd"),
											end.ToString("yyyy-MM-dd"));
			}

			// Filter out inactive SOPs 
			if (OnlyActiveSOPs)
				queryStr = String.Format("{0} AND active='1'", queryStr);


			// Build the actual query
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = String.Format(@"  SELECT sops.id AS id, sops.name AS name, sops.version AS version, sops.owner AS owner, sops.active AS active, sops.renewal AS renewal, training_log.tdate AS tdate, training_log.id as logID 
													FROM training_log
													JOIN sops ON training_log.sop_id = sops.id
													{0}
													ORDER BY tdate DESC;",
													queryStr);



				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TrainingLog newLog = new TrainingLog(int.Parse(reader["logID"].ToString()));
						newLog.Employee = new Employee(ee);

						newLog.SOP = new SOP()
						{
							Name = reader["name"].ToString(),
							Owner = reader["owner"].ToString(),
							Version = reader["version"].ToString(),
							Active = reader["active"].ToString() == "1",
							Renewal = Int32.Parse(reader["renewal"].ToString())
						};
						newLog.SOP.UpdateID(Int32.Parse(reader["id"].ToString()));
						newLog.TrainingDate = DateTime.ParseExact(reader["tdate"].ToString(), "yyyy-MM-dd", null);                        

						log.Add(newLog);
					}
				}
			}
			return log;
		}
		public static List<TrainingLog> GetTrainingLogBySOP(ref SOP sop, bool OnlyActiveEEs, DateTime? startDate, DateTime? endDate)
		{
			/*
				Query:
				SELECT employees.id AS id, employees.fname AS fname, employees.lname AS lname, employees.active AS active, 
					employees.shift_id AS shift_id, shifts.name as shift_name, employees.job_id AS job_id, jobs.name AS job_name, 
					training_log.tdate AS tdate
				FROM training_log
				JOIN employees ON training_log.employee_id=employees.id
				JOIN shifts ON employees.shift_id=shifts.id
				JOIN jobs ON employees.job_id=jobs.id
				WHERE training_log.sop_id=2;
			*/

			List<TrainingLog> log = new List<TrainingLog>();

			string queryStr = string.Format("WHERE training_log.sop_id='{0}'", sop.ID);

			// If the date range is not null, we're filtering by dates
			if (startDate.HasValue && endDate.HasValue)
			{
				DateTime start = startDate.Value;
				DateTime end = endDate.Value;

				queryStr = string.Format("{0} AND (tdate BETWEEN '{1}' AND '{2}')",
											queryStr,
											start.ToString("yyyy-MM-dd"),
											end.ToString("yyyy-MM-dd"));
			}

			// Filter out inactive SOPs 
			if (OnlyActiveEEs)
				queryStr = String.Format("{0} AND active='1'", queryStr);

			// Run the command
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = string.Format(@"  SELECT employees.id AS id, employees.fname AS fname, employees.lname AS lname, employees.active AS active, 
														employees.shift_id AS shift_id, shifts.name as shift_name, employees.job_id AS job_id, jobs.name AS job_name,
														training_log.tdate AS tdate, training_log.id as logID
													FROM training_log
													JOIN employees ON training_log.employee_id = employees.id
													JOIN shifts ON employees.shift_id = shifts.id
													JOIN jobs ON employees.job_id = jobs.id
													{0}
													ORDER BY tdate DESC;",
													queryStr);
				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TrainingLog newLog = new TrainingLog(int.Parse(reader["logID"].ToString()));
						newLog.Employee = new Employee()
						{
							FirstName = reader["fname"].ToString(),
							LastName = reader["lname"].ToString(),
							Active = reader["active"].ToString() == "1"
						};
						newLog.Employee.Job = reader["job_name"].ToString();
						newLog.Employee.UpdateJobID(int.Parse(reader["job_id"].ToString()));
						newLog.Employee.Shift = reader["shift_name"].ToString();
						newLog.Employee.UpdateShiftID(int.Parse(reader["shift_id"].ToString()));
						newLog.Employee.UpdateID(int.Parse(reader["id"].ToString()));

						newLog.SOP = sop;
						newLog.TrainingDate = DateTime.ParseExact(reader["tdate"].ToString(), "yyyy-MM-dd", null);

						log.Add(newLog);
					}
				}
			}
			return log;
		}
		public static List<TrainingLog> GetTrainingLogByDate(DateTime? startDate, DateTime? endDate, out long total, long limit = -1, long offset = 0)
		{
			List<TrainingLog> log = new List<TrainingLog>();

			// Returns a list of the last <limit> training entries added to the database.
			// 
			// It should be noted that <startDate> and <endDate> correspond to the date the training took place, 
			// NOT the date the training was added to the database.  We use the Primary Key (id column) for that
			// purpose, since SQLite3 guarantees AUTOIMCREMENT PRIMARY KEYs are table unique, so even a deleted
			// row id will not be reused, so every new addition to the training_log table will be a larger number
			//
			// total returns the count of rows that this query could return without the LIMIT / OFFSET parameters

			string queryStr = string.Empty;

			// If the date range is not null, we're filtering by dates
			if (startDate.HasValue && endDate.HasValue)
			{
				DateTime start = startDate.Value;
				DateTime end = endDate.Value;

				queryStr = string.Format("WHERE (tdate BETWEEN '{0}' AND '{1}')",
											start.ToString("yyyy-MM-dd"),
											end.ToString("yyyy-MM-dd"));
			}

			// Run the command
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Total
				cmd.CommandText = string.Format(@"  SELECT COUNT(*)
													FROM training_log
													{0};",
													queryStr);
				total = long.Parse(cmd.ExecuteScalar().ToString());

				// If limit is less than 1, return no limit (Empty string)
				string limitStr = string.Empty;

				if (limit > 0)
					limitStr = string.Format("LIMIT {0} OFFSET {1}", limit, offset);

				// Results
				cmd.CommandText = string.Format(@"  SELECT employees.id AS ee_id, employees.fname AS ee_fname, employees.lname AS ee_lname, employees.active AS ee_active, 
															employees.shift_id AS ee_shift_id, shifts.name as ee_shift_name, employees.job_id AS ee_job_id, jobs.name AS ee_job_name,
															sops.id AS sop_id, sops.name AS sop_name, sops.owner AS sop_owner, sops.version AS sop_version, sops.renewal AS sop_renewal,
															sops.active AS sop_active, 
															training_log.tdate AS tdate, training_log.id as logID
													FROM training_log
													JOIN employees ON training_log.employee_id = employees.id
													JOIN shifts ON employees.shift_id = shifts.id
													JOIN jobs ON employees.job_id = jobs.id
													JOIN sops ON training_log.sop_id = sops.id
													{0}
													ORDER BY tdate DESC, training_log.id DESC
													{1};",
													queryStr,
													limitStr);

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						TrainingLog newLog = new TrainingLog(int.Parse(reader["logID"].ToString()));

						// Employee ------------------------
						newLog.Employee = new Employee()
						{
							FirstName = reader["ee_fname"].ToString(),
							LastName = reader["ee_lname"].ToString(),
							Active = reader["ee_active"].ToString() == "1",
							Job = reader["ee_job_name"].ToString(),
							Shift = reader["ee_shift_name"].ToString()
						};
						newLog.Employee.UpdateJobID(int.Parse(reader["ee_job_id"].ToString()));
						newLog.Employee.UpdateShiftID(int.Parse(reader["ee_shift_id"].ToString()));
						newLog.Employee.UpdateID(int.Parse(reader["ee_id"].ToString()));

						// SOP -----------------------------
						newLog.SOP = new SOP()
						{
							Name = reader["sop_name"].ToString(),
							Owner = reader["sop_owner"].ToString(),
							Version = reader["sop_version"].ToString(),
							Active = reader["sop_active"].ToString() == "1",
							Renewal = Int32.Parse(reader["sop_renewal"].ToString())
						};
						newLog.SOP.UpdateID(Int32.Parse(reader["sop_id"].ToString()));

						// Training Date -------------------
						newLog.TrainingDate = DateTime.ParseExact(reader["tdate"].ToString(), "yyyy-MM-dd", null);

						log.Add(newLog);
					}
				}
			}
			return log;
		}
		// -- Training Is Due or Not --------------------------------------------------------------        
		public static List<TrainingLog> GetTrainingDue(GetTrainingQueryOptions opt)
		{
			/*  Returns all employess and sops, with a boolean column of is an SOP is overdue to be trained

				SELECT employees.fname, sops.name, training_log.tdate, COALESCE(training_log.tdate, 0) < date('now', '-'||sops.renewal||' month') As Due
				FROM employees
				JOIN sop_to_job ON employees.job_id = sop_to_job.job_id
				JOIN sops ON sop_to_job.sop_id=sops.id
				LEFT JOIN training_log ON sop_to_job.sop_id=training_log.sop_id AND employees.id=training_log.employee_id

				ORDER BY employees.lname



				SELECT DISTINCT employees.fname, sops.name, training_log.date, COALESCE(training_log.date, 0) < date('now', '-'||sops.renewal||' month') As Due
				FROM employees
				JOIN sop_to_job ON employees.job_id = sop_to_job.job_id
				JOIN sops ON sop_to_job.sop_id=sops.id
				LEFT JOIN (SELECT training_log.employee_id, 
											 training_log.sop_id
											 MAX(training_log.date) AS tDate
								 FROM training_log
								 GROUP BY training_log.employee_id) AS traininglog ON sop_to_job.sop_id=traininglog.sop_id AND employees.id=traininglog.employee_id
				WHERE Due=1 AND employees.fname='Abby' 
				ORDER BY employees.lname;

				;

			*/

			// Change the query string for use in sqlite3 like statements
			opt.CreateSQL();

			// Return List
			List<TrainingLog> Log = new List<TrainingLog>();

			// Build the Huge ass 'Where' query
			StringBuilder where = new StringBuilder();
			where.Append("WHERE ");
			// EE First Name
			where.Append("employees.fname LIKE '");
			where.Append(opt.EELikeFName + "' AND ");
			// EE Last Name
			where.Append("employees.lname LIKE '");
			where.Append(opt.EELikeLName + "' AND ");
			// EE Shift
			where.Append("shifts.name LIKE '");
			where.Append(opt.EEShift + "' AND ");
			// EE Job
			where.Append("jobs.name LIKE '");
			where.Append(opt.EEJob + "' AND ");
			// EE Active
			if (opt.EEActive)
				where.Append("employees.active='1' AND ");
			// SOP Active
			if (opt.SOPActive)
				where.Append("sops.active='1' AND ");
			// SOP Name
			where.Append("sops.name LIKE '");
			where.Append(opt.SOPLikeName + "' AND ");
			// SOP Version
			where.Append("sops.version LIKE '");
			where.Append(opt.SOPLikeVersion + "' AND ");
			// SOP Owner
			where.Append("sops.owner LIKE '");
			where.Append(opt.SOPLikeOwner + "' ");
			// TODO renewal isn't in query because I'm lazy today
			// This is trianing due, so only return training that is overdue
			//where.Append("Due=1 ");

			// Perform the query
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = string.Format(@"  
						SELECT employees.id AS eeid, employees.fname AS eefname, employees.lname AS eelname, employees.shift_id AS eeshift_id, shifts.name AS eeshift, employees.job_id AS eejob_id, jobs.name AS eejob, employees.active AS eeactive,
								sops.id AS sopid, sops.name AS sopname, sops.version AS sopversion, sops.owner AS sopowner, sops.renewal AS soprenewal, sops.active AS sopactive,
								training_log.tdate AS trainingdate, COALESCE(training_log.tdate, 0) < date('now', '-'||sops.renewal||' month') As Due
							FROM employees
							LEFT JOIN roles ON roles.employee_id=employees.id
							JOIN sop_to_job ON (employees.job_id = sop_to_job.job_id) OR (sop_to_job.job_id=roles.job_id)
							JOIN sops ON sop_to_job.sop_id=sops.id
							JOIN shifts ON shifts.id=employees.shift_id
							JOIN jobs ON jobs.id=employees.job_id
							LEFT JOIN training_log ON sop_to_job.sop_id=training_log.sop_id AND employees.id=training_log.employee_id
							{0}
							GROUP BY employees.id, sops.id
							ORDER BY employees.lname ASC;",
							where.ToString());

				// Get the results                    
				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						// Skip the record if it isn't due
						if (reader["Due"].ToString() == "1")
						{
							// Skip renewals == 0 (Should be never renew, not always renewing) that have a training date (because only 1 is required)
							if (reader["soprenewal"].ToString() == "0" && reader["trainingdate"].ToString() != string.Empty)
								continue;

							var newLog = new TrainingLog();

							// Create the employee
							newLog.Employee = new Employee(
													reader["eefname"].ToString(),
													reader["eelname"].ToString(),
													reader["eeshift"].ToString(),
													reader["eejob"].ToString(),
													reader["eeactive"].ToString() == "1",
													Int32.Parse(reader["eeid"].ToString()),
													Int32.Parse(reader["eeshift_id"].ToString()),
													Int32.Parse(reader["eejob_id"].ToString())
													);
							newLog.SOP = new SOP()
							{
								Name = reader["sopname"].ToString(),
								Owner = reader["sopowner"].ToString(),
								Version = reader["sopversion"].ToString(),
								Active = reader["sopactive"].ToString() == "1",
								Renewal = Int32.Parse(reader["soprenewal"].ToString())
							};
							newLog.SOP.UpdateID(Int32.Parse(reader["sopid"].ToString()));

							if (reader["trainingdate"] == null || String.IsNullOrWhiteSpace(reader["trainingdate"].ToString()))
								newLog.TrainingDate = null;
							else
								newLog.TrainingDate = DateTime.ParseExact(reader["trainingdate"].ToString(), "yyyy-MM-dd", null);

							Log.Add(newLog);
						}						
					}
				}
			}

			return Log;
		}
		public static List<Record> GetTrainingMatrix(List<Employee> employees)
		{
			// This is a crazy idea, a training matrix like this.  But we're going to make this thing happen
			// with some crazy solution I found online.  We should just generate this to csv directly
			// so it can be viewed with a simple data viewer like Excel
			//
			// Record is essentially a dynamically generated class.  We must ensure, then, that we are editing these
			// instances in the same order.

			List<Record> ret = new List<Record>();
			List<SOP> sops = GetAllSOPs();

			// Remove all Inactive SOPs
			sops.RemoveAll(x => !x.Active);

			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// One record per employee
				foreach (var ee in employees)
				{

					Record r = new Record();
					// Add the Normal Stuff
					r.AddProperty(new Property("Last Name", ee.LastName));
					r.AddProperty(new Property("First Name", ee.FirstName));
					r.AddProperty(new Property("Active", ee.Active));
					r.AddProperty(new Property("Job", ee.Job));
					r.AddProperty(new Property("Shift", ee.Shift));

					// Now add each SOP and query the database for it's completion date
					foreach (var sop in sops)
					{
						// Check to see if this SOP applies to the EE
						cmd.CommandText = string.Format(@"  SELECT (COUNT(*) > 0)
															FROM employees
															LEFT JOIN roles ON roles.employee_id=employees.id
															JOIN sop_to_job on (sop_to_job.job_id=employees.job_id OR sop_to_job.job_id=roles.job_id)
															WHERE sop_to_job.sop_id={0} AND employees.id={1}", sop.ID, ee.ID);
						bool SOPAppliesToEE = cmd.ExecuteScalar().ToString() == "1";

						// Get a Date if we have one
						cmd.CommandText = string.Format("SELECT MAX(tDate) FROM training_log WHERE employee_id={0} AND sop_id={1};", ee.ID, sop.ID);
						Object result = cmd.ExecuteScalar();

						if (result == DBNull.Value)
						{
							// No Date to Give
							if (SOPAppliesToEE)
								r.AddProperty(new Property(sop.Name, "Overdue"));
							else
								r.AddProperty(new Property(sop.Name, "N/A"));
						}
						else
						{
							// Add the SOP name and the date training was completed, and if it is overdue, mark it as so
							DateTime trainingDate = DateTime.ParseExact(result.ToString(), "yyyy-MM-dd", null);
							if (!SOPAppliesToEE)
							{
								// SOP does not apply, just send the latest date completed
								r.AddProperty(new Property(sop.Name, "N/A\n" + trainingDate.ToString("yyy-MM-dd")));
							}
							else if (sop.Renewal < 1)
							{
								// Not Overdue
								r.AddProperty(new Property(sop.Name, trainingDate.ToString("yyy-MM-dd")));
							}
							else
							{
								DateTime TrainingDue = DateTime.ParseExact(result.ToString(), "yyyy-MM-dd", null).AddMonths(sop.Renewal);
								TimeSpan diff = DateTime.Now.Subtract(TrainingDue);
								if (diff.TotalDays >= 0)
								{
									// Overdue
									r.AddProperty(new Property(sop.Name, "Overdue (" + diff.Days + " Days)\n" + trainingDate.ToString("yyyy-MM-dd")));
								}
								else
								{
									// Not Overdue
									r.AddProperty(new Property(sop.Name, trainingDate.ToString("yyy-MM-dd")));
								}
							}
						}
					}
					// Add the record to the list and keep goin
					ret.Add(r);
				}
			}

			return ret;
		}
		#endregion
		#region -- Database Set Helpers --
		// -- Shifts ------------------------------------------------------------------------------
		public static long GetOrAddShift(string shift)
		{
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT COUNT(*) FROM shifts WHERE name=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", shift);
				int count = Int32.Parse(cmd.ExecuteScalar().ToString());

				if (count == 0)
					return AddShift(shift); // Add Shift

				// Query Shift
				cmd.CommandText = "SELECT id FROM shifts WHERE name=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", shift);
				return Int32.Parse(cmd.ExecuteScalar().ToString());
			}
		}
		public static long SaveShift(ref Shift shift)
		{
			if (shift.ID < 1)
			{
				long newID = AddShift(shift.Name);
				shift.UpdateID(newID);
			}
			else
			{
				SQLiteConnection connection = Connect();
				
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{
					cmd.CommandText = "UPDATE shifts SET name=@paramName WHERE id=@paramID;";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@paramName", shift.Name);
					cmd.Parameters.AddWithValue("@paramID", shift.ID);
					cmd.ExecuteNonQuery();
				}
				
			}
			Modified = true;
			return shift.ID;
		}
		public static void DeleteShift(ref Shift shift)
		{
			// Deletes a shift from the database if it has no references in the employees table
			if (shift.ID < 1)
				throw new Exception("Cannot delete Shift if it isn't in the database!");

			long cnt = GetCountOfEmployeesWithShift(shift.ID);
			if (cnt > 0)
				throw new Exception("Cannot delete Shift if it has employees assigned to it!!");

			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "DELETE FROM shifts WHERE id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", shift.ID);
				cmd.ExecuteNonQuery();
				Modified = true;
			}
		}
		private static long AddShift(string shift)
		{
			// Adds a new shift to the database and returns its new ID
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "INSERT INTO shifts(name) VALUES(@param1);";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", shift);
				cmd.ExecuteNonQuery();
				Modified = true;
				return connection.LastInsertRowId;
			}                
		}
		// -- Jobs --------------------------------------------------------------------------------
		public static long GetOrAddJob(string job)
		{
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "SELECT COUNT(*) FROM jobs WHERE name=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", job);
				int count = int.Parse(cmd.ExecuteScalar().ToString());

				if (count == 0)
					return AddJob(job); // Add Shift

				// Query Shift
				cmd.CommandText = "SELECT id FROM jobs WHERE name=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", job);
				return long.Parse(cmd.ExecuteScalar().ToString());
			}
		}
		public static long SaveJob(ref Job job)
		{
			if (job.ID < 1)
			{
				long newID = AddJob(job.Name);
				job.UpdateID(newID);
			}
			else
			{
				SQLiteConnection connection = Connect();
				
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{
					cmd.CommandText = "UPDATE jobs SET name=@param1 WHERE id=@param2;";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@param1", job.Name);
					cmd.Parameters.AddWithValue("@param2", job.ID);
					cmd.ExecuteNonQuery();
					Modified = true;
				}
			}
			return job.ID;
		}
		public static void DeleteJob(ref Job job)
		{
			// Deletes a job from the database if it has no references in the employees table
			if (job.ID < 1)
				throw new Exception("Cannot delete Job if it isn't in the database!");

			long cnt = GetCountOfEmployeesWithJob(job.ID);
			if (cnt > 0)
				throw new Exception("Cannot delete Job if it has employees assigned to it!!");

			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "DELETE FROM jobs WHERE id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", job.ID);
				cmd.ExecuteNonQuery();
				Modified = true;
			}
		}
		private static long AddJob(string job)
		{
			// Adds a new shift to the database and returns its new ID
			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = "INSERT INTO jobs (name) VALUES(@param1);";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", job);
				cmd.ExecuteNonQuery();
				Modified = true;
				return connection.LastInsertRowId;
			}
		}
		// -- Employees ---------------------------------------------------------------------------
		public static void SaveEmployee(ref Employee em)
		{
			// Save an employee, which may or may not already exist in the database.

			// Start by saving / getting ids for job / shift
			em.UpdateJobID(GetOrAddJob(em.Job));
			em.UpdateShiftID(GetOrAddShift(em.Shift));

			// If the employee instance holds an id more than 0, it already has an id in the database and, thusly, already exists.
			if (em.ID < 1)
				AddEmployee(ref em);
			else
			{
				SQLiteConnection connection = Connect();
				
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{
					cmd.CommandText = "UPDATE employees SET lname=@pLName, fname=@pFName, active=@pActive, job_id=@pJobID, shift_id=@pShiftID WHERE id=@pID;";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@pLName", em.LastName);
					cmd.Parameters.AddWithValue("@pFName", em.FirstName);
					cmd.Parameters.AddWithValue("@pActive", em.Active ? 1 : 0);
					cmd.Parameters.AddWithValue("@pJobID", em.JobID);
					cmd.Parameters.AddWithValue("@pShiftID", em.ShiftID);
					cmd.Parameters.AddWithValue("@pID", em.ID);
					cmd.ExecuteNonQuery();
					Modified = true;
				}
			}

			// Now Employee has an ID
			if (em.ID < 1) // DEBUG
				throw new Exception("Employee Not Added??  In SaveEmployee(), after AddEmployee call.");

			// Once he has an ID, we can save his roles
			SaveRoleGlue(ref em);

		}
		private static void AddEmployee(ref Employee em)
		{
			// NOTE Make sure to update both shift_id and Job_id before calling this function
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = @"INSERT INTO employees (lname, fname, active, shift_id, job_id) 
												VALUES(@pLName, @pFName, @pActive, @pShiftID, @pJobID);";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@pLName", em.LastName);
				cmd.Parameters.AddWithValue("@pFName", em.FirstName);
				cmd.Parameters.AddWithValue("@pActive", em.Active ? 1 : 0);
				cmd.Parameters.AddWithValue("@pShiftID", em.ShiftID);
				cmd.Parameters.AddWithValue("@pJobID", em.JobID);
				cmd.ExecuteNonQuery();
				Modified = true;
				em.UpdateID(connection.LastInsertRowId);
			}
		}
		private static void SaveRoleGlue(ref Employee ee)
		{
			// Get IDs of roles
			List<long> RoleIDs = new List<long>(ee.Roles.Count);

			foreach (string job in ee.Roles)
				RoleIDs.Add(GetOrAddJob(job));

			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Next, delete all existing roles for this ee
				cmd.CommandText = "DELETE FROM roles WHERE employee_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", ee.ID);
				cmd.ExecuteNonQuery();

				// Now add them again
				foreach (long jobID in RoleIDs)
				{
					cmd.CommandText = "INSERT INTO roles(employee_id, job_id) VALUES(@pEEID,@pJobID);";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@pEEID", ee.ID);
					cmd.Parameters.AddWithValue("@pJobID", jobID);
					cmd.ExecuteNonQuery();
				}
			}
		}
		// -- SOPs --------------------------------------------------------------------------------
		public static void SaveSOP(ref SOP sop)
		{
			// Save an SOP, which may or may not exist in the database
			if (sop.ID < 1)
				AddSOP(ref sop);
			else
			{
				SQLiteConnection connection = Connect();
				
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{

					cmd.CommandText = "UPDATE sops SET name=@pName, version=@pVersion, owner=@pOwner, active=@pActive, renewal=@pRenewal WHERE id=@pID;";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@pName", sop.Name);
					cmd.Parameters.AddWithValue("@pVersion", sop.Version);
					cmd.Parameters.AddWithValue("@pOwner", sop.Owner);
					cmd.Parameters.AddWithValue("@pActive", sop.Active ? 1 : 0);
					cmd.Parameters.AddWithValue("@pRenewal", sop.Renewal);
					cmd.Parameters.AddWithValue("@pID", sop.ID);
					cmd.ExecuteNonQuery();
					Modified = true;
				}
			}

			// Now SOP has an ID
			if (sop.ID < 1) // DEBUG
				throw new Exception("SOP Not Added??  In SaveSOP(), after AddSOP call.");

			// Now, save the glue for sop_to_job
			SaveSOPToJobGlue(ref sop);
		}
		private static void SaveSOPToJobGlue(ref SOP sop)
		{
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// First, delete all entries linking to this SOP
				cmd.CommandText = "DELETE FROM sop_to_job WHERE sop_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", sop.ID);
				cmd.ExecuteNonQuery();

				// Now add each one back again
				foreach (string job in sop.AppliesToJobs)
				{
					// Find the job's ID
					long JobID = GetOrAddJob(job);

					// Create the link in sop_to_job
					cmd.CommandText = "INSERT INTO sop_to_job (sop_id, job_id) VALUES(@pID, @pJobID);";
					cmd.CommandType = System.Data.CommandType.Text;
					cmd.Parameters.AddWithValue("@pID", sop.ID);
					cmd.Parameters.AddWithValue("@pJobID", JobID);
					cmd.ExecuteNonQuery();
				}
			}
		}
		private static void AddSOP(ref SOP sop)
		{
			SQLiteConnection connection = Connect();
			
			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				cmd.CommandText = @"INSERT INTO sops(name, version, owner, active, renewal) 
												VALUES(@pName, @pVersion, @pOwner, @pActive, @pRenewal);";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@pName", sop.Name);
				cmd.Parameters.AddWithValue("@pVersion", sop.Version);
				cmd.Parameters.AddWithValue("@pOwner", sop.Owner);
				cmd.Parameters.AddWithValue("@pActive", sop.Active ? 1 : 0);
				cmd.Parameters.AddWithValue("@pRenewal", sop.Renewal);
				cmd.ExecuteNonQuery();
				Modified = true;
				sop.UpdateID(connection.LastInsertRowId);
			}
		}
		// -- TrainingLog -------------------------------------------------------------------------
		public static void AddTraining(ref List<TrainingLog> Logs)
		{
			if (Logs.Count < 1)
				return;

			SQLiteConnection connection = Connect();

			using (SQLiteTransaction transaction = connection.BeginTransaction())
			{
				using (SQLiteCommand cmd = new SQLiteCommand(connection))
				{
					foreach (TrainingLog log in Logs)
					{
						if (log.SOP.ID < 1)
							throw new Exception("AddTraining() Called with an unsaved SOP (without an ID)!");
						if (log.Employee.ID < 1)
							throw new Exception("AddTraining() called with an unsaved employee (without an ID)!");
						if (!log.TrainingDate.HasValue)
							throw new Exception("AddTraining() called without a training date!");

						cmd.CommandText = "INSERT INTO training_log (tdate, sop_id, employee_id) VALUES(@ptDate, @pSopID, @pEEID);";
						cmd.CommandType = System.Data.CommandType.Text;
						cmd.Parameters.AddWithValue("@ptDate", log.TrainingDate.Value.ToString("yyyy-MM-dd"));
						cmd.Parameters.AddWithValue("@pSopID", log.SOP.ID);
						cmd.Parameters.AddWithValue("@pEEID", log.Employee.ID);
						cmd.ExecuteNonQuery();
						
						// Update log ID
						cmd.CommandText = "SELECT last_insert_rowid();";
						log.UpdateID(int.Parse(cmd.ExecuteScalar().ToString()));
					}
				}
				transaction.Commit();
				Modified = true;
			}
		}
		#endregion
		#region -- Database Delete Operations --
		// Employee
		public static void DeleteEmployee(ref Employee em)
		{
			// Ensure employee to delete has an ID
			if (em.ID < 0)
				return;

			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Delete from tables: employees, roles, training_log
				cmd.CommandText = @"DELETE FROM employees WHERE id=@param1; DELETE FROM roles WHERE employee_id=@param1; DELETE FROM training_log WHERE employee_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", em.ID);

				cmd.ExecuteNonQuery();				
			}
			Modified = true;
		}
		// SOP
		public static void DeleteSOP(ref SOP sop)
		{
			// Ensure SOP to delete has an ID
			if (sop.ID < 0)
				return;

			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Delete from tables: sops, sop_to_job, training_log
				cmd.CommandText = @"DELETE FROM sops WHERE id=@param1; DELETE FROM sop_to_job WHERE sop_id=@param1; DELETE FROM training_log WHERE sop_id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", sop.ID);

				cmd.ExecuteNonQuery();
			}
			Modified = true;
		}
		// Training Event
		public static void DeleteTraining(ref TrainingLog log)
		{
			// Ensure Log to delete has an ID
			if (log.ID < 0)
				return;

			SQLiteConnection connection = Connect();

			using (SQLiteCommand cmd = new SQLiteCommand(connection))
			{
				// Delete from tables: training_log
				cmd.CommandText = @"DELETE FROM training_log WHERE id=@param1;";
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.Parameters.AddWithValue("@param1", log.ID);

				cmd.ExecuteNonQuery();
			}
			Modified = true;
		}

		#endregion

		// Utility
		public static bool DatabaseIsValid(string filename)
		{
			// returns true is this is a valid sqlite3 database file
			using (SQLiteConnection db = new SQLiteConnection(@"Data Source=" + filename + ";FailIfMissing=True;"))
			{
				try
				{
					db.Open();
					using (var transaction = db.BeginTransaction())
					{
						transaction.Rollback();
					}
				}
				catch (Exception)
				{
					return false;
				}
				return true;
			}
		}
		public static bool DatabaseMatchesOurSchema(string filename)
		{
			// Returns true if this database file has our database layout
			using (SQLiteConnection db = new SQLiteConnection(@"Data Source=" + filename + ";FailIfMissing=True;"))
			{
				try
				{
					db.Open();

					string sql = "SELECT count(*) FROM sqlite_master WHERE type='table' AND (name='employees' OR name='jobs' OR name='shifts' OR name='sop_to_job' OR name='sops' OR name='training_log' OR name='roles');";
					var cmd = new SQLiteCommand(sql, db);
					int tableCnt = Int32.Parse(cmd.ExecuteScalar().ToString());

					if (tableCnt != 7)  // Must have these 7 tables
						return false;
				}
				catch (Exception)
				{
					return false;
				}
				return true;
			}
		}

		// Huge Ass Query Option structure
		public struct GetTrainingQueryOptions
		{
			public string EELikeLName, EELikeFName, EEShift, EEJob, SOPLikeName, SOPLikeVersion, SOPLikeOwner;
			public bool EEActive, SOPActive;
			public int SOPLikeRenewal;

			public void CreateSQL()
			{
				EELikeLName += '%';
				EELikeFName += '%';
				SOPLikeName = '%' + SOPLikeName + '%';
				SOPLikeVersion += '%';
				SOPLikeOwner += '%';

				if (EEShift == "All")
					EEShift = "%";
				if (EEJob == "All")
					EEJob = "%";
			}
		}
	}
}
