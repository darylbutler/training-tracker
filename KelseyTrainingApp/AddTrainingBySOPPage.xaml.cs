﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for AddTrainingBySOPPage.xaml
    /// </summary>
    public partial class AddTrainingBySOPPage : Page
    {
        // Lists from Database
        List<SOP> SOPs;
        List<SOP> filteredSOPs;
        List<Employee> Employees;
        ObservableCollection<Employee> filteredEmployees;
        List<string> Shifts;

        // Currently selected Instances
        SOP SelectedSOP;                     // Only one selected SOP
        List<Employee> SelectedEmployees;   // We can have lots of employees

        public AddTrainingBySOPPage()
        {
            InitializeComponent();

            // Load Data
            SOPs = Database.GetAllSOPs();
            filteredSOPs = new List<SOP>(SOPs);            
            Shifts = new List<string>();
            Shifts.Add("All");
            Shifts.AddRange(Database.GetAllShifts());

            Employees = Database.GetAllEmployees();            
            filteredEmployees = new ObservableCollection<Employee>(Employees);

            // Bind Data
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;    
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;

            // Empty SOP is selected
            SelectedSOP = new SOP();
            SelectedEmployees = new List<Employee>();

            // Begin Filtered to filter the inactive SOPs out of the query
            FilterSOPChanged(null, null);
        }

        // -- Select SOP Code-behind
        private void FilterSOPChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in SOPs select d;

            if (!String.IsNullOrWhiteSpace(FilterSOPByNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Name.StartsWith(FilterSOPByNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByVersionBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Version.StartsWith(FilterSOPByVersionBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByOwnerBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Owner.StartsWith(FilterSOPByOwnerBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByRenewalBox.Text))
            {
                int newRenewal = -1;
                if (Int32.TryParse(FilterSOPByRenewalBox.Text, out newRenewal))
                    tmpFiltered = from d in tmpFiltered where d.Renewal > newRenewal select d;
            }

            if (FilterSOPByActiveBox.IsChecked ?? false)
                tmpFiltered = from d in tmpFiltered where d.Active == true select d;

            filteredSOPs = tmpFiltered.ToList<SOP>();
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
        }
        private void FilterSOPByActiveBox_Click(object sender, RoutedEventArgs e)
        {
            FilterSOPChanged(sender, null);
        }
        private void FindSOPsDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            FindSOPsDataGrid.SelectedIndex = 0;
        }
        private void FindSOPsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindSOPsDataGrid.SelectedIndex < 0)
                return;

            SelectedSOP = (SOP)FindSOPsDataGrid.SelectedItem;
            Database.GetSOPAppliesToJobs(ref SelectedSOP);
            SelectedSOPNameLabel.Text = SelectedSOP.Name + " // " + SelectedSOP.Version;
            FilterEEChanged(null, null);
        }

        // -- Select EE Code-behind
        private void FilterEEChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in Employees select d;

            // If there's a selected SOP, make sure we only show employees that match the 'AppliesToJob' field
            // Do NOT do this if the Filter EEs By Course Jobs checkbox isn't checked
            if ((FilterEEsBySOPJobs.IsChecked ?? true) && FindSOPsDataGrid.SelectedIndex > -1)
                tmpFiltered = from d in tmpFiltered where SelectedSOP.AppliesToJobs.Contains(d.Job) select d;

            // Normal Filtering
            if (!String.IsNullOrWhiteSpace(FilterEEByFNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.FirstName.StartsWith(FilterEEByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterEEByLNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.LastName.StartsWith(FilterEEByLNameBox.Text, true, null) select d;
            if (FilterEEByShiftBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Shift == FilterEEByShiftBox.SelectedItem.ToString() select d;
            if (FilterEEByActive.IsChecked ?? true)
                tmpFiltered = from d in tmpFiltered where d.Active select d;

            filteredEmployees = new ObservableCollection<Employee>(tmpFiltered.ToList<Employee>());
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
        }
        private void FilterEEShiftBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterEEChanged(null, null);
        }
        private void FilterEEByActive_Click(object sender, RoutedEventArgs e)
        {
            FilterEEChanged(null, null);
        }
        private void FilterEEsBySOPJobs_Click(object sender, RoutedEventArgs e)
        {
            // Reload the fitler
            FilterEEChanged(null, null);
        }
        private void FindEmployeesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindEmployeesDataGrid.SelectedIndex < 0)
                return;

            // Add the selected items
            SelectedEmployees.Clear();
            foreach (var d in FindEmployeesDataGrid.SelectedItems)
                SelectedEmployees.Add((Employee)d);

            // Update the Selected Label
            var firstNames = from ee in SelectedEmployees select (ee.FirstName + ' ' + ee.LastName.First() + '.');
            SelectedEENamesLabel.Text = String.Join(", ", firstNames);
        }
        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            // Ensure there's a selected Date
            if (TrainingDatePicker.SelectedDate == null)
            {
                MessageBox.Show("Please Select a Date.", "Error Trying to Add Training", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (TrainingDatePicker.SelectedDate > DateTime.Now)
            {
                MessageBox.Show("Training Date has a selected date greater than today!\nYou can't complete training that hasn't been completed yet, dude -_-", "What's wrong with you", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // Ensure there's a selected SOP (A SOP That exists in the Database (ID > 0))
            if (FindSOPsDataGrid.SelectedIndex < 0 && SelectedSOP != null && SelectedSOP.ID > 0)
                return;
            // Build the selected Employees list
            FindEmployeesDataGrid_SelectionChanged(null, null);
            // Exit if no employees selected
            if (SelectedEmployees.Count == 0)
                return;

            // Let's do the database dirty
            List<TrainingLog> Logs = new List<TrainingLog>(SelectedEmployees.Count);
            foreach (Employee ee in SelectedEmployees)
            {
                TrainingLog log = new TrainingLog();
                log.SOP = SelectedSOP;
                log.Employee = ee;
                log.TrainingDate = TrainingDatePicker.SelectedDate ?? DateTime.Now;
                Logs.Add(log);
            }
            Database.AddTraining(ref Logs);
            int cntAdded = Logs.Count;

            // Inform user of success
            string msg = "Submission complete.  A total of " + cntAdded.ToString() + (cntAdded == 1 ? " entry was" : " entries were") + " added to the database!";
            MessageBox.Show(msg, "Great Success!", MessageBoxButton.OK, MessageBoxImage.Asterisk);

            // Unselect everything in the Employees Datagrid so the user knows the query was successful.                        
            FindEmployeesDataGrid.SelectedItems.Clear();
            FindEmployeesDataGrid_SelectionChanged(null, null);

            // Reload data and any active filters
            FilterEEChanged(null, null);    
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
        }        
    }
}
