﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for EditUsersPage.xaml
    /// </summary>
    public partial class EditEmployeesPage : Page
    {       
        // Lists from Database
        List<Employee> Employees;
        List<string> Jobs, Shifts;

        // filter lists
        List<Employee> filteredEmployees;
        List<string> filterJobs, filterShifts;

        Employee CurrentEmployee;   // Currently editing employee

        // For Datagrid sorting saving
        string SortPath;
        SortDescription SortDesc;

        bool PageLoaded = false;

        public EditEmployeesPage()
        {
            InitializeComponent();
            Jobs = new List<string>();
            Shifts = new List<string>();
            Employees = new List<Employee>();

            // Fill the above Arrays
            LoadData();

            // Create the list for filtering (So Employees remains unchanged).
            filteredEmployees = new List<Employee>(Employees);
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;

            filterShifts = new List<string>();
            filterShifts.Add("All");
            filterShifts.AddRange(Shifts);
            filterJobs = new List<string>();
            filterJobs.Add("All");
            filterJobs.AddRange(Jobs);

            // Connect the Lists to their componenets
            // Filtered is different, Add an 'All' entry
            FilterByJobBox.ItemsSource = filterJobs;
            FilterByShiftBox.ItemsSource = filterShifts;
            EditShiftBox.ItemsSource = Shifts;
            EditJobBox.ItemsSource = Jobs;
            AllJobsBox.ItemsSource = Jobs;

            // Reset defaults
            PageLoaded = true;
            EmployeeChanged.IsChecked = false;
            NewEmployeeBtn_Click(null, null);   // Always start with a new Employee            
        }

        private void LoadData()
        {
            // Loads the data from the Database
            Jobs = Database.GetAllJobs();
            Shifts = Database.GetAllShifts();
            Employees = Database.GetAllEmployees();
        }

        private void FilterByChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in Employees select d;

            if (!String.IsNullOrWhiteSpace(FilterByFNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.FirstName.StartsWith(FilterByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByLNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.LastName.StartsWith(FilterByLNameBox.Text, true, null) select d;
            if (FilterByShiftBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Shift == FilterByShiftBox.SelectedItem.ToString() select d;
            if (FilterByJobBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Job.StartsWith(FilterByJobBox.SelectedItem.ToString(), true, null) select d;
            if (FilterByActiveBox.IsChecked ?? false)
                tmpFiltered = from d in tmpFiltered where d.Active == true select d;

            filteredEmployees = tmpFiltered.ToList<Employee>();
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;

            // Restore sort path
            if (SortDesc != null && SortDesc.PropertyName != null)
                FindEmployeesDataGrid.Items.SortDescriptions.Add(SortDesc);
        }
        private void FilterByActiveBox_Checked(object sender, RoutedEventArgs e)
        {
            FilterByChanged(sender, null);
        }
        private void FilterByShiftBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterByChanged(sender, null);
        }

        private void EditBoxChanged(object sender, TextChangedEventArgs e)
        {
            if (EmployeeChanged != null)
                EmployeeChanged.IsChecked = true;
            // Reset Buttons
            EnableBtns();
        }
        private void EditComboChanged(object sender, SelectionChangedEventArgs e)
        {
            EditBoxChanged(e, null);
        }
        private void EditActiveChanged(object sender, RoutedEventArgs e)
        {
            EditBoxChanged(e, null);
        }
        private void FindEmployeesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Reset Buttons
            EnableBtns();
        }       
        private bool CanSaveUser()
        {
            // Returns true if all required fields aren't empty
            return  !String.IsNullOrWhiteSpace(EditFNameBox.Text) &&
                    !String.IsNullOrWhiteSpace(EditLNameBox.Text) &&
                    EditShiftBox.SelectedIndex > -1 &&
                    EditJobBox.SelectedIndex > -1;
        }
        private void ReportWhyCannotSaveProblems()
        {
            string Error = "Cannot save Employee.  Not all required fields have been completed.\nProblems:\n";

            if (String.IsNullOrWhiteSpace(EditLNameBox.Text))
                Error += "\n\t- Employee's Last Name is an Empty String";
            if (String.IsNullOrWhiteSpace(EditFNameBox.Text))
                Error += "\n\t- Employee's First Name is an Empty String";
            if (EditShiftBox.SelectedIndex < 0)
                Error += "\n\t- Employee has no Shift selected";
            if (EditJobBox.SelectedIndex < 0)
                Error += "\n\t- Employee has no Job selected";

            MessageBox.Show(Error, "Employee cannot be saved", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        private bool PromptToSaveUser()
        {            
            // Ask the user if we want to save employee before continuing
            var result = MessageBox.Show("Employee changes have not been saved to the database.\nDo you wish to save them before continuing?", "Warning!  Unsaved Changes!", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Cancel)
                return false;
            if (result == MessageBoxResult.Yes)
            {
                // User wants to save.  Make sure he can save before we return true
                if (CanSaveUser())
                    SaveEmployee(CurrentEmployee);
                else
                {
                    ReportWhyCannotSaveProblems();
                    return false; // Cannot save, return false to prevent changes from being lost
                }
            }
            // Both Yes and No return true from here (Because if we return false we are stopping the iniating action from occuring.  Thus-> Cancel returns false).  OTher wise, we only
            // Check for Yes to see if we need to save first before continuing
            return true;
        }
        private bool EEExistsAlreadyInDatabase()
        {
            var cnt = from ee in Employees
                      where string.Compare(ee.FirstName, EditFNameBox.Text, true) == 0 &&
                            string.Compare(ee.LastName, EditLNameBox.Text, true) == 0 &&
                            string.Compare(ee.Job, EditJobBox.Text, true) == 0
                      select ee;
            return cnt.Count() > 0;
        }
        private void EditEmployeeBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will move the selected employee to the lower portion for him to be edited.  
            // If cancel, then return
            if (EmployeeChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            if (FindEmployeesDataGrid.SelectedIndex == -1)
                return;

            LoadEmployee((Employee)FindEmployeesDataGrid.SelectedItem);
            // Reset Buttons
            EnableBtns();
            // Focus First Edit Box for Editing
            EditLNameBox.Focus();   
        }
        private void ResetEmployeeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeeChanged.IsChecked ?? true)
            {
                // Discard Changes Mode
                LoadEmployee(CurrentEmployee);
                // Reset Buttons
                EnableBtns();
                // Focus First Edit Box for Editing
                EditLNameBox.Focus();
            }
            else
            {
                // ---- Delete Employee Mode
                var res = MessageBox.Show("Are you sure you wish to delete this Employee?  This will remove all occurances of this Employee throughout the entire database.\n"+
                    "It will remove: (1)the employee, (2) all associated roles, and (3) ALL training records that point to this Employee.\n" + 
                    "Click Yes if you are sure this is want you want to do.  Note that this will NOT save the database, so it can be undone by exiting -without- saving.",
                    "You Sure About This?", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                if (res != MessageBoxResult.Yes)
                    return;

                // -- Return if no Employee Selected
                if (FindEmployeesDataGrid.SelectedIndex == -1)
                    return;

                // -- Call delete function in Database
                Employee ee = (Employee)FindEmployeesDataGrid.SelectedItem;
                Database.DeleteEmployee(ref ee);

                // -- Reset top Employee view
                // Save the -selected- Item
                var selected = FindEmployeesDataGrid.SelectedIndex;
                // Reloaded Employees for filter
                Employees = Database.GetAllEmployees();
                filteredEmployees = new List<Employee>(Employees);
                FindEmployeesDataGrid.ItemsSource = filteredEmployees;
                // Restore sort path
                if (SortDesc != null && SortDesc.PropertyName != null)
                    FindEmployeesDataGrid.Items.SortDescriptions.Add(SortDesc);
                // Restore Selected Item
                if (selected > -1 && selected < FindEmployeesDataGrid.Items.Count)  // Restore same index if at beginning or middle of list
                    FindEmployeesDataGrid.SelectedIndex = selected;    
                else if (selected - 1 < FindEmployeesDataGrid.Items.Count)          // Restore last index if at end of list
                    FindEmployeesDataGrid.SelectedIndex = selected - 1;
                else                                                                // Restore -1 because wtf
                    FindEmployeesDataGrid.SelectedIndex = -1;                

                // -- Load the a new Employee
                NewEmployeeBtn_Click(null, null);

                // -- Focus the Top Panel
                FindEmployeesDataGrid.Focus();

                // -- Reset Buttons
                EnableBtns();
            }
            
        }
        private void NewEmployeeBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will create a new employee for the user to edit at the bottom  
            // If they have edited a user aleady withouit saving, promt to save
            // if cancel, then return
            if (EmployeeChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            // Create a new employee and load him
            CurrentEmployee = new Employee();
            CurrentEmployee.Active = true;  // Default to creating an active Employee
            LoadEmployee(CurrentEmployee);

            // Reset Buttons
            EnableBtns();
            // Focus First Edit Box for Editing
            EditLNameBox.Focus();
        }
        private void SaveEmployeeBtn_Click(object sender, RoutedEventArgs e)
        {
            // Check if EE is valid
            if (!CanSaveUser())
            {
                ReportWhyCannotSaveProblems();
                return;
            }

            // Check for EE duplicate, but allow it (Only ask when creating a new ee)
            if (CurrentEmployee.ID < 1 && EEExistsAlreadyInDatabase())
            {
                var result = MessageBox.Show("Employee with same Last Name, First Name, and Job type exists in the Database Already.  Do you still wish to add this Employee anyway?", "Duplicate Employee Found", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                    return;
            }

            SaveEmployee(CurrentEmployee);
            NewEmployeeBtn_Click(sender, e);
        }

        private void EditBox_KeyUp(object sender, KeyEventArgs e)
        {
            // If Key pressed is enter and all the new edit boxes are not empty and not saved...
            if (e.Key == Key.Enter &&
                (EmployeeChanged.IsChecked ?? false) &&
                !String.IsNullOrWhiteSpace(EditLNameBox.Text) &&
                !String.IsNullOrWhiteSpace(EditFNameBox.Text) &&
                EditShiftBox.SelectedIndex > -1 &&
                EditJobBox.SelectedIndex > -1)
            {
                // Shortcut to save New Employee
                SaveEmployeeBtn_Click(null, null);
            }
        }

        private void FindEmployeesDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            // Save the sort path so we can restore it after a grid reload
            string sortPath = e.Column.SortMemberPath;
            string prevSortPath = SortPath;

            if (sortPath == prevSortPath)
            {
                SortDesc = new SortDescription(sortPath, ListSortDirection.Descending);
            }
            else {
                SortDesc = new SortDescription(sortPath, ListSortDirection.Ascending);
            }
            SortPath = sortPath;
        }

        private void LoadEmployee(Employee em)
        {
            // Loads an employee into the bottom / editing portion of the UI
            EditLNameBox.Text = em.LastName;
            EditFNameBox.Text = em.FirstName;
            EditShiftBox.Text = em.Shift;
            EditJobBox.Text = em.Job;
            EditActiveBox.IsChecked = em.Active;
            EmployeeChanged.IsChecked = false;
            EditRolesBox.Items.Clear();

            foreach (var role in em.Roles)
                EditRolesBox.Items.Add(role);

            // Save the employee so we know how to save
            CurrentEmployee = em;
        }
        private void EditAddJobBtn_Click(object sender, RoutedEventArgs e)
        {
            // If nothing to add is selected, then return
            if (AllJobsBox.SelectedIndex == -1)
                return;

            // If the Job to add already exists in the AppliesToJobs box, then return
            if (EditRolesBox.Items.Contains(AllJobsBox.SelectedItem.ToString()))
                return;

            // Add the Role
            EditRolesBox.Items.Add(AllJobsBox.SelectedItem.ToString());

            // Mark the SOP as changed
            EditBoxChanged(e, null);
        }
        private void EditRemoveJobBtn_Click(object sender, RoutedEventArgs e)
        {
            // If nothing is selected to remove from the AppliesToJobs box, then return
            if (EditRolesBox.SelectedIndex == -1)
                return;

            // Remove the item from the list
            EditRolesBox.Items.RemoveAt(EditRolesBox.SelectedIndex);

            // Mark the SOP as changed
            EditBoxChanged(e, null);
        }
        private void SaveEmployee(Employee em)
        {
            // Copy the edited information back into the employee instance
            em.LastName = EditLNameBox.Text;
            em.FirstName = EditFNameBox.Text;
            em.Shift = EditShiftBox.Text;
            em.Job = EditJobBox.Text;
            em.Active = EditActiveBox.IsChecked ?? false;

            em.Roles = new List<string>(EditRolesBox.Items.Count);
            foreach (var s in EditRolesBox.Items)
                em.Roles.Add(s.ToString());

            // Save the selected Item
            var selected = FindEmployeesDataGrid.SelectedIndex;

            // Pass that instance to the database to be inserted / saved
            Database.SaveEmployee(ref em);

            // Reset changed bit
            EmployeeChanged.IsChecked = false;

            // Reloaded Employees for filter
            Employees = Database.GetAllEmployees();
            filteredEmployees = new List<Employee>(Employees);
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;

            // Restore sort path
            if (SortDesc != null && SortDesc.PropertyName != null)
                FindEmployeesDataGrid.Items.SortDescriptions.Add(SortDesc);

            // Restore Selected Item
            FindEmployeesDataGrid.SelectedIndex = selected;
        }
        private void EnableBtns()
        {
            // Prevent this from being called before Page is loaded
            if (!PageLoaded)
                return;

            // Edit Btn
            EditEmployeeBtn.IsEnabled = (FindEmployeesDataGrid.SelectedIndex > -1);

            // New SOP
            NewEmployeeBtn.IsEnabled = true;

            // Discard Changes
            if (EmployeeChanged.IsChecked ?? true)
            {
                // Discard Changes Mode
                ResetEmployeeBtn.IsEnabled = true;
                ResetEmployeeBtnText.Text = "Discard Changes";
            }
            else if (EditEmployeeBtn.IsEnabled) // If an employee is selected, we can delete it
            {
                // Delete Employee Mode
                ResetEmployeeBtn.IsEnabled = true;
                ResetEmployeeBtnText.Text = "Delete Employee";
            }
            else
                ResetEmployeeBtn.IsEnabled = false;

            // Save SOP
            SaveEmployeeBtn.IsEnabled = EmployeeChanged.IsChecked ?? true;
        }
    }
}
