﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for EditJobsPage.xaml
    /// </summary>
    public partial class EditJobsPage : Page
    {
        List<Job> Jobs;
        List<Job> filteredJobs;

        Job CurrentJob;
        public EditJobsPage()
        {
            InitializeComponent();

            // Fill the Shifts array
            Jobs = Database.GetAllJobsForEditing();
            FilterBox_TextChanged(null, null);
            NewItemBtn_Click(null, null);
        }

        private void FilterBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (!String.IsNullOrWhiteSpace(FilterBox.Text))
            {
                var filter = from s in Jobs where s.Name.StartsWith(FilterBox.Text, true, null) select s;
                filteredJobs = new List<Job>(filter);
            }
            else
                filteredJobs = new List<Job>(Jobs);

            Grid.ItemsSource = filteredJobs;
        }
        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EditItemBtn.IsEnabled = (Grid.SelectedIndex > -1);
        }
        private void ShiftNameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ItemChanged.IsChecked = true;

            if (CurrentJob != null)
                SaveItemBtn.IsEnabled = true;
        }
        private bool PromptToSaveUser()
        {
            // Ask the user if we want to save before continuing
            var result = MessageBox.Show("Changes have not been saved to the database.\nDo you wish to save them before continuing?", "Warning!  Unsaved Changes!", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Cancel)
                return false;
            if (result == MessageBoxResult.Yes)
                SaveItem(CurrentJob);

            // Both Yes and No return true from here (Because if we return false we are stopping the iniating action from occuring.  Thus-> Cancel returns false).  OTher wise, we only
            // Check for Yes to see if we need to save first before continuing
            return true;
        }

        private void LoadItem(Job job)
        {
            // Loads a job into the bottom / editing portion of the UI
            ShiftNameBox.Text = job.Name;
            EmployeesAssignedLabel.Text = Database.GetCountOfEmployeesWithJob(job.ID).ToString();
            ItemChanged.IsChecked = false;

            // Save the item
            CurrentJob = job;
        }
        private void SaveItem(Job job)
        {
            // Copy the edited information back into the job instance
            job.Name = ShiftNameBox.Text;

            // Pass that instance to the database to be inserted / saved
            Database.SaveJob(ref job);

            // Reset changed bit
            ItemChanged.IsChecked = false;

            // Reload jobs for filter
            Jobs = Database.GetAllJobsForEditing();
            FilterBox_TextChanged(null, null);
        }
        private void EditItemBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will move the selected item to the lower portion for him to be edited.  
            // If cancel, then return
            if (ItemChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            if (Grid.SelectedIndex == -1)
                return;

            LoadItem((Job)Grid.SelectedItem);
            SaveItemBtn.IsEnabled = true;
            DeleteItemBtn.IsEnabled = (EmployeesAssignedLabel.Text == "0");
        }
        private void NewItemBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will create a new Job for the user to edit at the bottom  
            // If they have edited a job aleady withouit saving, promt to save
            // if cancel, then return
            if (ItemChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            // Create a new job and load it
            CurrentJob = new Job(0, String.Empty);
            LoadItem(CurrentJob);
            SaveItemBtn.IsEnabled = false;
            DeleteItemBtn.IsEnabled = false;
        }
        private void SaveItemBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveItem(CurrentJob);
            NewItemBtn_Click(null, null);
        }
        private void DeleteItemBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentJob.ID < 1)
            {
                MessageBox.Show("Please edit a job before trying to delete it!");
                return;
            }
            Database.DeleteJob(ref CurrentJob);

            // Relaod the shifts
            Jobs = Database.GetAllJobsForEditing();
            FilterBox_TextChanged(null, null);

            ItemChanged.IsChecked = false;
            DeleteItemBtn.IsEnabled = false;
            NewItemBtn_Click(null, null);
        }
    }
}
