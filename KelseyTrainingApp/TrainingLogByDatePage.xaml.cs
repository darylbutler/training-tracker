﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingLogByDatePage.xaml
    /// </summary>   
    public partial class TrainingLogByDatePage : Page
    {
        long RowsPerView;                                   // For paging, indicates how many rows to ask the database for
        long CurrentPageNumber, TotalPageCount, TotalRows;  // For Paging
        bool ReportLoaded;                                  // true once we've run a query
        DateTime? QueryStartDate, QueryEndDate;             // Remember dates for paging
        List<TrainingLog> Log;

        public TrainingLogByDatePage()
        {
            InitializeComponent();
            
            CurrentPageNumber = 0;
            TotalPageCount = 0;
            ReportLoaded = false;
        }
        long CalculateRowsPerView()
        {
            long rpv = (long)Math.Round(ReportGrid.ActualHeight / 20d);
            return rpv <= 0 ? 1 : rpv;  // Disallow a 0 RowsPerView, since we use it in the denominator for Page Count Calculation
        }
        // New Filter Stuff
        private void FilterByYearValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            // Only allow numbers
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void FilterDatesInYearBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterDatesByYear.IsChecked = true;
        }
        private void FilterDateRange_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterDatesByRange.IsChecked = true;
        }

        private void UpdateUIWithNewLog()
        {
            // Update the UI
            ReportGrid.ItemsSource = Log;

            // Page Count
            CurrentPage.Text = (CurrentPageNumber + 1).ToString();
            TotalPages.Text = TotalPageCount.ToString();

            // Paging Buttons
            PrevPageBtn.IsEnabled = ReportLoaded && (CurrentPageNumber > 0);
            NextPageBtn.IsEnabled = ReportLoaded && (CurrentPageNumber < TotalPageCount - 1);
            FirstPageBtn.IsEnabled = PrevPageBtn.IsEnabled;
            LastPageBtn.IsEnabled = NextPageBtn.IsEnabled;

            // Restore focus to grid
            ReportGrid.Focus();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            // Return if nothing selected
            if (!ReportLoaded || ReportGrid.SelectedIndex < 0)
                return;

            // Prompt to delete
            if (MessageBox.Show("Are you sure you want to delete this training record?", "You Sure About This?", 
                MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                return;

            // Call database to delete
            TrainingLog training_log = (TrainingLog)ReportGrid.SelectedItem;
            Database.DeleteTraining(ref training_log);

            // -- Update top panel
            // Edge Case: If this page only had one record, then we need to reload the previous page.  Otherwise, we can just reload this page
            if (Log.Count == 1)
            {
                CurrentPageNumber = Math.Max(0, CurrentPageNumber - 1);
                TotalPageCount = Math.Max(1, TotalPageCount - 1);
            }
            // Reload page
            Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate,
                                                    out TotalRows, RowsPerView,
                                                    CurrentPageNumber * RowsPerView
                                                    );
            // And Update the UI
            UpdateUIWithNewLog();
        }
        private void ReportGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DeleteBtn.IsEnabled = ReportGrid.SelectedIndex >= 0;
        }

        // Get the Report
        private void RunBtn_Click(object sender, RoutedEventArgs e)
        {
            // Check on missing user input and setup the Start/End dates, if needed
            if (FilterDatesByYear.IsChecked ?? false)
            {
                if (string.IsNullOrWhiteSpace(FilterDatesInYearBox.Text) || FilterDatesInYearBox.Text.Length != 4)
                {
                    MessageBox.Show("When filtering by Year, the Year value cannot be empty and must be atleast 4 characters long!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                
                QueryStartDate = new DateTime(int.Parse(FilterDatesInYearBox.Text), 1, 1);          // January 1st of the year
                QueryEndDate = QueryStartDate.Value.AddDays(364);                                   // Not 365, because that would be Jan 1st of the next year
            }
            else if (FilterDatesByRange.IsChecked ?? false)
            {
                // No dates selected
                if (!FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    MessageBox.Show("When filtering by Date Range, Atleast the Start or the End dates must have a value selected!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                // Single day query
                if (FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                }
                else if (FilterDateRange_EndDate.SelectedDate.HasValue && !FilterDateRange_StartDate.SelectedDate.HasValue)
                {
                    FilterDateRange_StartDate.SelectedDate = FilterDateRange_EndDate.SelectedDate;
                }
                // End date before start date
                else if (FilterDateRange_StartDate.SelectedDate > FilterDateRange_EndDate.SelectedDate)
                {
                    var result = MessageBox.Show("The date selected as the starting date is later than the date selected as the ending date.\nDo you want to swap the dates?", "Dates are Inverted", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.No)
                        return;
                    else
                    {
                        DateTime? temp = FilterDateRange_EndDate.SelectedDate;
                        FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                        FilterDateRange_StartDate.SelectedDate = temp;
                    }
                }

                QueryStartDate  = FilterDateRange_StartDate.SelectedDate;
                QueryEndDate    = FilterDateRange_EndDate.SelectedDate;
            }
            else
                QueryStartDate = QueryEndDate = null;

            // Perform the Query
            RowsPerView = CalculateRowsPerView();
            Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate, 
                                                out TotalRows, RowsPerView
                                                );
            ReportLoaded = true;

            // Pagins
            CurrentPageNumber = 0;
            TotalPageCount = TotalRows / RowsPerView;
            if (TotalRows % RowsPerView > 0)
                TotalPageCount++;

            UpdateUIWithNewLog();
        }
        private void FirstPageBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ReportLoaded)
            {
                CurrentPageNumber = 0;
                Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate,
                                                    out TotalRows, RowsPerView,
                                                    CurrentPageNumber * RowsPerView
                                                    );
                UpdateUIWithNewLog();
            }
        }
        private void LastPageBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ReportLoaded)
            {
                CurrentPageNumber = Math.Max(0, TotalPageCount - 1);
                Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate,
                                                    out TotalRows, RowsPerView,
                                                    CurrentPageNumber * RowsPerView
                                                    );
                UpdateUIWithNewLog();
            }
        }
        private void NextPageBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ReportLoaded && CurrentPageNumber + 1 < TotalPageCount)
            {
                CurrentPageNumber++;
                Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate, 
                                                    out TotalRows, RowsPerView, 
                                                    CurrentPageNumber * RowsPerView
                                                    );
                UpdateUIWithNewLog();
            }
        }
        private void PrevPageBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ReportLoaded && CurrentPageNumber > 0)
            {
                CurrentPageNumber--;
                Log = Database.GetTrainingLogByDate(QueryStartDate, QueryEndDate,
                                                    out TotalRows, RowsPerView,
                                                    CurrentPageNumber * RowsPerView
                                                    );
                UpdateUIWithNewLog();
            }
        }
    }
}
