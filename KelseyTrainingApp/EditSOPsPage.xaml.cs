﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for EditSOPsPage.xaml
    /// </summary>
    public partial class EditSOPsPage : Page
    {
        // Lists of ALL SOPs and Jobs
        List<SOP> SOPs;
        List<string> Jobs;

        // Filtered List of SOPs
        List<SOP> filteredSOPs;

        // SOP being editing on bottom portion
        SOP CurrentSOP;

        // For Datagrid sorting saving
        string SortPath;
        SortDescription SortDesc;

        bool PageLoaded = false;

        public EditSOPsPage()
        {
            InitializeComponent();

            // Load all the data
            SOPs = Database.GetAllSOPs();
            Jobs = Database.GetAllJobs();

            // Setup the filter link
            filteredSOPs = new List<SOP>(SOPs);
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
            AllJobsBox.ItemsSource = Jobs;

            // Reset bits to default
            PageLoaded = true;
            SOPChanged.IsChecked = false;
            NewSOPBtn_Click(null, null);
        }

        private void FilterByChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in SOPs select d;

            if (!String.IsNullOrWhiteSpace(FilterByNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Name.StartsWith(FilterByNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByVersionBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Version.StartsWith(FilterByVersionBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByOwnerBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Owner.StartsWith(FilterByOwnerBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByRenewalBox.Text))
            {
                int newRenewal = -1;
                if (Int32.TryParse(FilterByRenewalBox.Text, out newRenewal))
                    tmpFiltered = from d in tmpFiltered where d.Renewal > newRenewal select d;
            }
                
            if (FilterByActiveBox.IsChecked ?? false)
                tmpFiltered = from d in tmpFiltered where d.Active == true select d;

            filteredSOPs = tmpFiltered.ToList<SOP>();
            FindSOPsDataGrid.ItemsSource = filteredSOPs;

            // Restore sort path
            if (SortDesc != null && SortDesc.PropertyName != null)
                FindSOPsDataGrid.Items.SortDescriptions.Add(SortDesc);
        }
        private void FilterByActiveBox_Checked(object sender, RoutedEventArgs e)
        {
            FilterByChanged(sender, null);
        }

        private void EditBoxChanged(object sender, TextChangedEventArgs e)
        {
            if (SOPChanged != null)
                SOPChanged.IsChecked = true;

            // Reset Buttons
            EnableBtns();
        }
        private void EditActiveChanged(object sender, RoutedEventArgs e)
        {
            EditBoxChanged(e, null);
        }
        private void FindSOPsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Reset Buttons
            EnableBtns();
        }
        private bool PromptToSaveUser()
        {
            // Ask the user if we want to save employee before continuing
            var result = MessageBox.Show("SOP changes have not been saved to the database.\nDo you wish to save them before continuing?", "Warning!  Unsaved Changes!", MessageBoxButton.YesNoCancel);

            if (result == MessageBoxResult.Cancel)
                return false;
            if (result == MessageBoxResult.Yes)
                SaveSOP(CurrentSOP);

            // Both Yes and No return true from here (Because if we return false we are stopping the iniating action from occuring.  Thus-> Cancel returns false).  OTher wise, we only
            // Check for Yes to see if we need to save first before continuing
            return true;
        }
        private bool SOPExistsAlreadyInDatabase()
        {
            var cnt = from ss in SOPs
                      where ss.Name == EditNameBox.Text && ss.Version == EditVersionBox.Text
                      select ss;
            return cnt.Count() > 0;
        }
        private void EditSOPBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will move the selected employee to the lower portion for him to be edited.  
            // If cancel, then return
            if (SOPChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            if (FindSOPsDataGrid.SelectedIndex == -1)
                return;

            LoadSOP((SOP)FindSOPsDataGrid.SelectedItem);
            // Reset Buttons
            EnableBtns();
            // Focus first edit box for Editing
            EditNameBox.Focus(); 
        }
        private void ResetSOPBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SOPChanged.IsChecked ?? true)
            {
                // Discard Changes Mode
                LoadSOP(CurrentSOP);
                // Reset Buttons
                EnableBtns();
                // Focus first edit box for Editing
                EditNameBox.Focus();
            }
            else
            {
                // ---- Delete SOP Mode
                var res = MessageBox.Show("Are you sure you wish to delete this Course?  This will remove all occurances of this Course throughout the entire database.\n" +
                    "It will remove: (1)this Course and (2) ALL training records that point to this Course.\n" +
                    "Click Yes if you are sure this is want you want to do.  Note that this will NOT save the database, so it can be undone by exiting -without- saving.",
                    "You Sure About This?", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

                if (res != MessageBoxResult.Yes)
                    return;

                // -- Return if No SOP selected
                if (FindSOPsDataGrid.SelectedIndex == -1)
                    return;

                // -- Call Database Delete Function
                SOP sop = (SOP)FindSOPsDataGrid.SelectedItem;
                Database.DeleteSOP(ref sop);

                // -- Reload top view
                // Save the selected Item
                int selected = FindSOPsDataGrid.SelectedIndex;
                // Reloaded SOPs for filter
                SOPs = Database.GetAllSOPs();
                filteredSOPs = new List<SOP>(SOPs);
                FindSOPsDataGrid.ItemsSource = filteredSOPs;
                // Restore sort path
                if (SortDesc != null && SortDesc.PropertyName != null)
                    FindSOPsDataGrid.Items.SortDescriptions.Add(SortDesc);
                // Restore Selected Item
                if (selected > -1 && selected < FindSOPsDataGrid.Items.Count)  // Restore same index if at beginning or middle of list
                    FindSOPsDataGrid.SelectedIndex = selected;
                else if (selected - 1 < FindSOPsDataGrid.Items.Count)          // Restore last index if at end of list
                    FindSOPsDataGrid.SelectedIndex = selected - 1;
                else                                                           // Restore -1 because wtf
                    FindSOPsDataGrid.SelectedIndex = -1;

                // -- Load a new SOP
                NewSOPBtn_Click(null, null);

                // -- Focus Top Panel
                FindSOPsDataGrid.Focus();

                // -- Reset Buttons
                EnableBtns();
            }
        }
        private void NewSOPBtn_Click(object sender, RoutedEventArgs e)
        {
            // This will create a new SOP for the user to edit at the bottom  
            // If they have edited a SOP aleady withouit saving, promt to save
            // if cancel, then return
            if (SOPChanged.IsChecked ?? true)
                if (!PromptToSaveUser())
                    return;

            // Create a new employee and load him
            CurrentSOP = new SOP();
            // New SOPs are usually active SOPs 
            CurrentSOP.Active = true;
            LoadSOP(CurrentSOP);
            // Reset Buttons
            EnableBtns();
            // Focus first edit box for Editing
            EditNameBox.Focus();   
        }
        private void CopySOPBtn_Click(object sender, RoutedEventArgs e)
        {
            // Provide a copy of the selected SOP for editing, but reset the SOPs ID to -1 so it will be added
            // to the database with a different ID when it is saved
            if (FindSOPsDataGrid.SelectedIndex == -1)
                return;

            // Load the SOP we want to make a copy of
            LoadSOP((SOP)FindSOPsDataGrid.SelectedItem);
            // Reset the ID
            CurrentSOP.UpdateID(-1);
            // Mark as NOT saved
            SOPChanged.IsChecked = true;
            // Reset Buttons
            EnableBtns();
            // Focus first edit box for Editing
            EditNameBox.Focus();            
        }
        private void SaveSOPBtn_Click(object sender, RoutedEventArgs e)
        {
            // Check if the edit field are valid before allowing a save
            //
            // We must have a name
            if (String.IsNullOrWhiteSpace(EditNameBox.Text))
            {
                MessageBox.Show("Name cannot be empty or whitespace.");
                EditNameBox.Focus();
                return;
            }
            // Renewal must be a number
            int renewal = -1;
            if (!Int32.TryParse(EditRenewalBox.Text, out renewal))
            {
                MessageBox.Show("Value in Renewal must be a number!");
                EditRenewalBox.Focus();
                return;
            }
            // Renewal must not be negative
            if (renewal < 0)
            {
                MessageBox.Show("Value in Renewal too small.  Use 0 to indicate no renewal period, otherwise\nthis number must be positive.");
                EditRenewalBox.Focus();
                return;
            }

            // Check for sop duplicate, but allow it (Only ask when creating a new sop)
            if (CurrentSOP.ID < 1 && SOPExistsAlreadyInDatabase())
            {
                var result = MessageBox.Show("Course with same Name and Version exists in the Database Already.  Do you still wish to add this Course anyway?", "Duplicate Course Found", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                    return;
            }

            SaveSOP(CurrentSOP);
            NewSOPBtn_Click(sender, e);

            // Reset Buttons
            EnableBtns();
        }
        private void EditBox_KeyUp(object sender, KeyEventArgs e)
        {
            // If Key pressed is enter and all the new edit boxes are not empty and not saved...
            if (e.Key == Key.Enter &&
                (SOPChanged.IsChecked ?? false))
            {
                // Shortcut to save New SOP
                SaveSOPBtn_Click(null, null);
            }
        }
        private void FindSOPsDataGrid_Sorting(object sender, DataGridSortingEventArgs e)
        {
            // Save the sort path so we can restore it after a grid reload
            string sortPath = e.Column.SortMemberPath;
            string prevSortPath = SortPath;

            if (sortPath == prevSortPath)
            {
                SortDesc = new SortDescription(sortPath, ListSortDirection.Descending);
            }
            else {
                SortDesc = new SortDescription(sortPath, ListSortDirection.Ascending);
            }
            SortPath = sortPath;
        }
        private void LoadSOP(SOP sop)
        {
            // First, Load the AppliesToJobs array from the database
            Database.GetSOPAppliesToJobs(ref sop);

            // Fill in the edit form
            EditNameBox.Text = sop.Name;
            EditVersionBox.Text = sop.Version;
            EditOwnerBox.Text = sop.Owner;
            EditRenewalBox.Text = sop.Renewal.ToString();
            EditActiveBox.IsChecked = sop.Active;
            EditSOPToJobRelationshipsBox.Items.Clear();

            foreach (string s in sop.AppliesToJobs)
                EditSOPToJobRelationshipsBox.Items.Add(s);

            SOPChanged.IsChecked = false;
            CurrentSOP = sop;

            // Reset Buttons
            EnableBtns();
        }
        private void SaveSOP(SOP sop)
        {
            // Assume all data is valid (EditRenewalBox.Text should be an int)
            sop.Name = EditNameBox.Text;
            sop.Version = EditVersionBox.Text;
            sop.Owner = EditOwnerBox.Text;
            sop.Renewal = Int32.Parse(EditRenewalBox.Text);
            sop.Active = EditActiveBox.IsChecked ?? false;

            sop.AppliesToJobs.Clear();
            foreach (var s in EditSOPToJobRelationshipsBox.Items)
                sop.AppliesToJobs.Add(s.ToString());

            // Save the selected Item
            var selected = FindSOPsDataGrid.SelectedIndex;

            // Call the database to save the SOP
            Database.SaveSOP(ref sop);

            // Save successful
            // Reset changed bit
            SOPChanged.IsChecked = false;

            // Reloaded Employees for filter
            SOPs = Database.GetAllSOPs();
            filteredSOPs = new List<SOP>(SOPs);
            FindSOPsDataGrid.ItemsSource = filteredSOPs;

            // Restore sort path
            if (SortDesc != null && SortDesc.PropertyName != null)
                FindSOPsDataGrid.Items.SortDescriptions.Add(SortDesc);

            // Restore Selected Item
            FindSOPsDataGrid.SelectedIndex = selected;

            // Reset Buttons
            EnableBtns();
        }
        private void EditAddJobBtn_Click(object sender, RoutedEventArgs e)
        {
            // If nothing to add is selected, then return
            if (AllJobsBox.SelectedIndex == -1)
                return;

            // If the Job to add already exists in the AppliesToJobs box, then return
            if (EditSOPToJobRelationshipsBox.Items.Contains(AllJobsBox.SelectedItem.ToString()))
                return;

            // Add the Job
            EditSOPToJobRelationshipsBox.Items.Add(AllJobsBox.SelectedItem.ToString());

            // Mark the SOP as changed
            EditBoxChanged(e, null);
        }
        private void EditRemoveJobBtn_Click(object sender, RoutedEventArgs e)
        {
            // If nothing is selected to remove from the AppliesToJobs box, then return
            if (EditSOPToJobRelationshipsBox.SelectedIndex == -1)
                return;

            // Remove the item from the list
            EditSOPToJobRelationshipsBox.Items.RemoveAt(EditSOPToJobRelationshipsBox.SelectedIndex);

            // Mark the SOP as changed
            EditBoxChanged(e, null);
        }

        private void EnableBtns()
        {
            // Prevent this from being called before Page is loaded
            if (!PageLoaded)
                return;

            // Edit Btn
            EditSOPBtn.IsEnabled = (FindSOPsDataGrid.SelectedIndex > -1);

            // New SOP
            NewSOPBtn.IsEnabled = true;

            // Copy SOP
            CopySOPBtn.IsEnabled = CurrentSOP.ID > 0;

            // Discard Changes
            if (SOPChanged.IsChecked ?? true)
            {
                // Discard Changes Mode
                ResetSOPBtn.IsEnabled = true;
                ResetSOPBtnText.Text = "Discard";
            }
            else if (EditSOPBtn.IsEnabled) // If SOP selected
            {
                // Delete SOP Mode
                ResetSOPBtn.IsEnabled = true;
                ResetSOPBtnText.Text = "Delete SOP";
            }
            else
                ResetSOPBtn.IsEnabled = false;

            // Save SOP
            SaveSOPBtn.IsEnabled = SOPChanged.IsChecked ?? true;
        }        
    }
}
