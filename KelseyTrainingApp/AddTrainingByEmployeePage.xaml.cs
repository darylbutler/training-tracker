﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for AddTrainingByEmployeePage.xaml
    /// </summary>
    public partial class AddTrainingByEmployeePage : Page
    {
        List<Employee> Employees;
        List<Employee> filteredEmployees;
        List<SOP> SOPs;
        List<SOP> filteredSOPs;
        List<string> Shifts;
        List<string> Jobs;

        Employee SelectedEmployee;
        List<SOP> SelectedSOPs;
        public AddTrainingByEmployeePage()
        {
            InitializeComponent();

            // Load Data
            Employees = Database.GetAllEmployees();
            filteredEmployees = new List<Employee>(Employees);
            SOPs = Database.GetAllSOPs();
            filteredSOPs = new List<SOP>(SOPs);

            Jobs = new List<string>();
            Jobs.Add("All");
            Jobs.AddRange(Database.GetAllJobs());
            Shifts = new List<string>();
            Shifts.Add("All");
            Shifts.AddRange(Database.GetAllShifts());

            // Data linking
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
            FilterEEByJobBox.ItemsSource = Jobs;
            FilterEEByJobBox.SelectedIndex = 0;
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;

            // Defaults
            SelectedEmployee = new Employee();
            SelectedSOPs = new List<SOP>();

            // Filter the initial list to only active SOPs
            FilterSOPChanged(null, null);
        }

        // -- Select EE Code-behind
        private void FilterEEChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in Employees select d;

            if (!String.IsNullOrWhiteSpace(FilterEEByFNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.FirstName.StartsWith(FilterEEByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterEEByLNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.LastName.StartsWith(FilterEEByLNameBox.Text, true, null) select d;
            if (FilterEEByShiftBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Shift == FilterEEByShiftBox.SelectedItem.ToString() select d;
            if (FilterEEByJobBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Job.StartsWith(FilterEEByJobBox.SelectedItem.ToString(), true, null) select d;

            // Added Active to filter
            if (FilterEEByActiveBox.IsChecked ?? true)
                tmpFiltered = from d in tmpFiltered where d.Active select d;

            filteredEmployees = tmpFiltered.ToList<Employee>();
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
        }
        private void FilterEEByActiveBox_Click(object sender, RoutedEventArgs e)
        {
            FilterEEChanged(null, null);
        }
        private void FilterEEShiftBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterEEChanged(sender, null);
        }
        private void FindEmployeesDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // Select something
            FindEmployeesDataGrid.SelectedIndex = 0;
        }
        private void FindEmployeesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindEmployeesDataGrid.SelectedIndex > -1)
            {
                SelectedEmployee = (Employee)FindEmployeesDataGrid.SelectedItem;
                SelectedEENameLabel.Text = SelectedEmployee.FirstName + ' ' + SelectedEmployee.LastName + " // " + SelectedEmployee.Job + " on " + SelectedEmployee.Shift;
            }
        }

        // -- Select SOPs Code-behind
        private void FilterSOPChanged(object sender, TextChangedEventArgs e)
        {
            // TODO Should we be filtering by which SOPs appy to the selected employee's job?  That might change how quickly we can filter.

            var tmpFiltered = from d in SOPs select d;

            if (!String.IsNullOrWhiteSpace(FilterSOPByNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Name.StartsWith(FilterSOPByNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByVersionBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Version.StartsWith(FilterSOPByVersionBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByOwnerBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Owner.StartsWith(FilterSOPByOwnerBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterSOPByRenewalBox.Text))
            {
                int newRenewal = -1;
                if (Int32.TryParse(FilterSOPByRenewalBox.Text, out newRenewal))
                    tmpFiltered = from d in tmpFiltered where d.Renewal > newRenewal select d;
            }

            if (FilterSOPByActiveBox.IsChecked ?? false)
                tmpFiltered = from d in tmpFiltered where d.Active == true select d;

            filteredSOPs = tmpFiltered.ToList<SOP>();
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
        }
        private void FilterSOPByActiveBox_Click(object sender, RoutedEventArgs e)
        {
            FilterSOPChanged(sender, null);
        }
        private void FindSOPsDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // Select Something
            FindSOPsDataGrid.SelectedIndex = 0;
        }
        private void FindSOPsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindSOPsDataGrid.SelectedIndex < 0)
                return;

            // Add the selected items
            SelectedSOPs.Clear();
            foreach (var d in FindSOPsDataGrid.SelectedItems)
                SelectedSOPs.Add((SOP)d);

            // Update the Selected Label
            SelectedSOPNamesLabel.Text = SelectedSOPs.Count + " Course" + (SelectedSOPs.Count == 1 ? " " : "s ") + "selected.";
        }


        // The big kahuna!
        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            // Ensure there's a selected Date
            if (TrainingDatePicker.SelectedDate == null)
            {
                MessageBox.Show("Please Select a Date.", "Error Trying to Add Training", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (TrainingDatePicker.SelectedDate > DateTime.Now)
            {
                MessageBox.Show("Training Date has a selected date greater than today!\nYou can't complete training that hasn't been completed yet, dude -_-", "What's wrong with you", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // Ensure there's some selected SOPs
            if (SelectedSOPs.Count < 1)
            {
                MessageBox.Show("Please select atleast 1 Course to mark as trained.", "Error Trying to Add Training", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // Ensure there's a selected Employee
            if (FindEmployeesDataGrid.SelectedIndex < 0)
            {
                MessageBox.Show("Please select an Employee that was Trained.", "Error Trying to Add Training", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Let's do the database dirty
            List<TrainingLog> Logs = new List<TrainingLog>(SelectedSOPs.Count);
            foreach (SOP sop in SelectedSOPs)
            {
                TrainingLog log = new TrainingLog();
                log.SOP = sop;
                log.Employee = SelectedEmployee;
                log.TrainingDate = TrainingDatePicker.SelectedDate ?? DateTime.Now;
                Logs.Add(log);
            }
            Database.AddTraining(ref Logs);
            int cntAdded = Logs.Count;

            // Inform user of success
            string msg = "Submission complete.  A total of " + cntAdded.ToString() + (cntAdded == 1 ? " entry was" : " entries were") + " added to the database!";
            MessageBox.Show(msg, "Great Success!", MessageBoxButton.OK, MessageBoxImage.Asterisk);

            // Unselect everything in the SOPs Datagrid so the user knows the query was successful.
            FindSOPsDataGrid.SelectedIndex = 0;
        }
    }
}
