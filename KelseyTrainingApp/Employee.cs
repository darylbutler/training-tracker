﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace KelseyTrainingApp
{
    public class Employee
    {
        private long id, shift_id, job_id;
        private Employee ee;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Shift { get; set; }   // NOTE that these override the applicable Job_ID / Shift_ID
        public string Job { get; set; }     // Because the application edit based on text (Not much data entry), so it should be correct
        public List<string> Roles { get; set; } // These are secondary jobs.  They are used to assign more SOPs to an employee
        public bool Active { get; set; }
        public long ID { get { return id; } }
        public long ShiftID { get { return shift_id; } }
        public long JobID { get { return job_id; } }

        public Employee() { id = -1; shift_id = -1; job_id = -1; Roles = new List<string>(); }
        public Employee(string fname, string lname, string shift, string job, bool active, long _id, long shiftid, long jobid)
        {
            this.FirstName = fname;
            this.LastName = lname;
            this.Shift = shift;
            this.Job = job;
            this.Active = active;
            this.id = _id;
            this.shift_id = shiftid;
            this.job_id = jobid;
            Roles = new List<string>();
        }

        public Employee(Employee ee)
        {
            this.ee = ee;
        }

        public void UpdateID(long ID)
        {
            this.id = ID;
        }
        public void UpdateShiftID(long ID)
        {
            this.shift_id = ID;
        }
        public void UpdateJobID(long ID)
        {
            this.job_id = ID;
        }

        
    }
}
