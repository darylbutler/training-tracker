﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingLogByEmployeePage.xaml
    /// </summary>
    public partial class TrainingLogByEmployeePage : Page
    {
        List<Employee> Employees;
        List<Employee> filteredEmployees;
        List<TrainingLog> Log;
        List<string> Shifts;
        List<string> Jobs;
        Employee SelectedEmployee;

        public TrainingLogByEmployeePage()
        {
            InitializeComponent();

            // Load Data
            Employees = Database.GetAllEmployees();
            filteredEmployees = new List<Employee>(Employees);

            Jobs = new List<string>();
            Jobs.Add("All");
            Jobs.AddRange(Database.GetAllJobs());
            Shifts = new List<string>();
            Shifts.Add("All");
            Shifts.AddRange(Database.GetAllShifts());

            // Data linking
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
            FilterEEByJobBox.ItemsSource = Jobs;
            FilterEEByJobBox.SelectedIndex = 0;
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;

            // Defaults
            SelectedEmployee = new Employee();
        }

        #region -- Select EE Code-behind ----------------------------------------------------------------------------------------
        private void FilterEEChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in Employees select d;

            if (!String.IsNullOrWhiteSpace(FilterEEByFNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.FirstName.StartsWith(FilterEEByFNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterEEByLNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.LastName.StartsWith(FilterEEByLNameBox.Text, true, null) select d;
            if (FilterEEByShiftBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Shift == FilterEEByShiftBox.SelectedItem.ToString() select d;
            if (FilterEEByJobBox.SelectedIndex > 0)
                tmpFiltered = from d in tmpFiltered where d.Job.StartsWith(FilterEEByJobBox.SelectedItem.ToString(), true, null) select d;

            // Added Active to filter
            if (FilterEEByActiveBox.IsChecked ?? true)
                tmpFiltered = from d in tmpFiltered where d.Active select d;

            filteredEmployees = tmpFiltered.ToList<Employee>();
            FindEmployeesDataGrid.ItemsSource = filteredEmployees;
        }
        private void FilterEEByActiveBox_Click(object sender, RoutedEventArgs e)
        {
            FilterEEChanged(null, null);
        }
        private void FilterEEShiftBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterEEChanged(sender, null);
        }
        private void FindEmployeesDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            // Select something
            FindEmployeesDataGrid.SelectedIndex = 0;
        }
        private void FindEmployeesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindEmployeesDataGrid.SelectedIndex > -1)
            {
                SelectedEmployee = (Employee)FindEmployeesDataGrid.SelectedItem;
                SelectedEENameLabel.Text = SelectedEmployee.FirstName + ' ' + SelectedEmployee.LastName + " // " + SelectedEmployee.Job + " on " + SelectedEmployee.Shift;
            }
        }


        #endregion

        // New Filter Stuff
        private void FilterByYearValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            // Only allow numbers
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void FilterDatesInYearBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterDatesByYear.IsChecked = true;
        }
        private void FilterDateRange_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterDatesByRange.IsChecked = true;
        }

        // Run the Query
        private void RunBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedEmployee == null || SelectedEmployee.ID < 1)
                return;

            // Check on Filters
            if (FilterDatesByYear.IsChecked ?? false)
            {
                if (String.IsNullOrWhiteSpace(FilterDatesInYearBox.Text) || FilterDatesInYearBox.Text.Length != 4)
                {
                    MessageBox.Show("When filtering by Year, the Year value cannot be empty and must be atleast 4 characters long!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            if (FilterDatesByRange.IsChecked ?? false)
            {
                // No dates selected
                if (!FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    MessageBox.Show("When filtering by Date Range, Atleast the Start or the End dates must have a value selected!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                // Single day query
                if (FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                }
                else if (FilterDateRange_EndDate.SelectedDate.HasValue && !FilterDateRange_StartDate.SelectedDate.HasValue)
                {
                    FilterDateRange_StartDate.SelectedDate = FilterDateRange_EndDate.SelectedDate;
                }
                // End date before start date
                else if (FilterDateRange_StartDate.SelectedDate > FilterDateRange_EndDate.SelectedDate)
                {
                    var result = MessageBox.Show("The date selected as the starting date is later than the date selected as the ending date.\nDo you want to swap the dates?", "Dates are Inverted", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.No)
                        return;
                    else
                    {
                        DateTime? temp = FilterDateRange_EndDate.SelectedDate;
                        FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                        FilterDateRange_StartDate.SelectedDate = temp;
                    }
                }
            }

            // Actually do the query
            if (FilterDatesAll.IsChecked ?? false)
                Log = Database.GetTrainingLogByEmployee(ref SelectedEmployee, (FilterByActiveSOPs.IsChecked ?? true), null, null);
            else if (FilterDatesByYear.IsChecked ?? false)
            {
                DateTime startDate = new DateTime(Int32.Parse(FilterDatesInYearBox.Text), 1, 1);    // January 1st of the year
                DateTime endDate = startDate.AddDays(364);                                          // Not 365, because that would be Jan 1st of the next year

                Log = Database.GetTrainingLogByEmployee(ref SelectedEmployee, (FilterByActiveSOPs.IsChecked ?? true), startDate, endDate);
            }
            else
            {
                Log = Database.GetTrainingLogByEmployee(ref SelectedEmployee, (FilterByActiveSOPs.IsChecked ?? true), 
                    FilterDateRange_StartDate.SelectedDate, FilterDateRange_EndDate.SelectedDate);
            }

            // Update the grid
            ReportDataGrid.ItemsSource = Log;
        }
        // Add Employee Double Click shortcut to run the Query on that employee
        private void FindEmployeesDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RunBtn_Click(null, null);
        }

        // Print Screenshot
        private void PrintBtn_Click(object sender, RoutedEventArgs e)
        {
            // Exit if not query has been run
            if (Log == null || Log.Count < 1)
                return;

            string name = DateTime.Now.ToString("yyyy-MM-dd") + " -- TrainingLog By " + SelectedEmployee.LastName + ", " + SelectedEmployee.FirstName;

            Log.First().Employee = SelectedEmployee;    // Fill in the first employee for Report Viewer
            var win = new TrainingLogByEmployeeReportWindow(Log);
            win.ReportWindow.Title = name;
            win.ShowDialog();


        }
        // Export Report to csv for easy importing into Excel or a better report viewer
        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            // Exit if not query has been run
            if (Log == null || Log.Count < 1)
                return;

            string suggestedFileName = (DateTime.Now.ToString("yyyy-MM-dd") + " -- TrainingLog By " + SelectedEmployee.LastName + ", " + SelectedEmployee.FirstName).Replace('"', '\'');

            System.Windows.Forms.SaveFileDialog Savedlg = new System.Windows.Forms.SaveFileDialog();
            Savedlg.FileName = suggestedFileName;
            Savedlg.Filter = "CSV Files (*.csv)|*.csv|All Files|*.*";
            Savedlg.Title = "Export to CSV file...";

            if (Savedlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string filename = Savedlg.FileName;
            List<string> file = new List<string>(Log.Count + 1);

            // Create the CSV
            // TODO Write a class to do this, this is dirty.  I feel dirty.  Reflection should make this pretty easy List<PropertyInfo> = typeof(t).GetProperties();

            // Header line
            file.Add("Training Date,Course Name,Course Version,Course Owner,Course Is Active?");
            foreach (var t in Log)
            {
                // Each trainig log is a line
                StringBuilder line = new StringBuilder();

                line.Append(t.TrainingDate.HasValue ? ((DateTime)t.TrainingDate).ToString("yyyy-MM-dd") : String.Empty);
                line.Append(',');
                line.Append(quote(t.SOP.Name));
                line.Append(',');
                line.Append(quote(t.SOP.Version));
                line.Append(',');
                line.Append(quote(t.SOP.Owner));
                line.Append(',');
                line.Append(t.SOP.Active);                

                file.Add(line.ToString());
            }

            // Write this out to the selected file
            System.IO.File.WriteAllLines(filename, file);
        }
        private string quote(string s)
        {
            return '"' + s.Replace("\"", "\"\"") + '"';
        }

        private void ReportDataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DeleteBtn.IsEnabled = ReportDataGrid.SelectedIndex >= 0;
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            // Return if nothing selected
            if (ReportDataGrid.SelectedIndex < 0)
                return;

            // Prompt to delete
            if (MessageBox.Show("Are you sure you want to delete this training record?", "You Sure About This?",
                MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                return;

            // Call database to delete
            TrainingLog training_log = (TrainingLog)ReportDataGrid.SelectedItem;
            Database.DeleteTraining(ref training_log);

            // -- Update Bottom Panel
            int selected = ReportDataGrid.SelectedIndex;
            RunBtn_Click(null, null);
            if (selected > -1 && selected < ReportDataGrid.Items.Count)  // Restore same index if at beginning or middle of list
                ReportDataGrid.SelectedIndex = selected;
            else if (selected - 1 < ReportDataGrid.Items.Count)          // Restore last index if at end of list
                ReportDataGrid.SelectedIndex = selected - 1;
            else                                                         // Restore -1 because wtf
                ReportDataGrid.SelectedIndex = -1;

            // And Update the UI
            ReportDataGrid.Focus();
        }
    }
}
