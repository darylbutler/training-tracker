﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KelseyTrainingApp
{
    public class SOP
    {
        long id;

        public long ID { get { return id; } }

        public string Name { get; set; }
        public string Version { get; set; }
        public string Owner { get; set; }
        public bool Active { get; set; }
        public int Renewal { get; set; }

        public void UpdateID(long _id) { id = _id; }

        public List<string> AppliesToJobs;
    }
}
