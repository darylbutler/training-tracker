﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingDuePage.xaml
    /// </summary>
    public partial class TrainingDuePage : Page
    {
        const string SelectAllStr = "All";
        List<string> Shifts;
        List<string> Jobs;
        List<TrainingLog> QueriedLog;
        List<string> SOPNames;

        public TrainingDuePage()
        {
            InitializeComponent();

            // Get Data
            Shifts = new List<string>();
            Shifts.Add(SelectAllStr);
            Shifts.AddRange(Database.GetAllShifts());
            Jobs = new List<string>();
            Jobs.Add(SelectAllStr);
            Jobs.AddRange(Database.GetAllJobs());
            var sops = Database.GetAllSOPs();
            SOPNames = (from s in sops select s.Name).ToList();


            // Data Binding
            FilterEEByJobBox.ItemsSource = Jobs;
            FilterEEByJobBox.SelectedIndex = 0;
            FilterEEByShiftBox.ItemsSource = Shifts;
            FilterEEByShiftBox.SelectedIndex = 0;
            FilterSOPByNameBox.ItemsSource = SOPNames;

            // Do Not Assign QueriedLog yet
        }

        private async void ApplyFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            // Show the busy window...
            BusyWindow win = new BusyWindow();
            win.Owner = App.Current.MainWindow;
            win.Show();

            // Build Query structure to send to the database
            Database.GetTrainingQueryOptions opt = new Database.GetTrainingQueryOptions();

            if (String.IsNullOrWhiteSpace(FilterSOPByRenewalBox.Text))
                FilterSOPByRenewalBox.Text = "0";
            int renewal = 0;
            if (!Int32.TryParse(FilterSOPByRenewalBox.Text, out renewal) || renewal < 0)
            {
                MessageBox.Show("Filter by Renewal value cannot be parsed or is too small.  Use a value of 0 for no renewal filtering.");
                FilterSOPByRenewalBox.Focus();
                return;
            }

            opt.EELikeFName = FilterEEByFNameBox.Text;
            opt.EELikeLName = FilterEEByLNameBox.Text;
            opt.EEShift     = FilterEEByShiftBox.Text;
            opt.EEJob       = FilterEEByJobBox.Text;
            opt.EEActive    = FilterEEByActiveBox.IsChecked ?? true;
            opt.SOPActive = FilterSOPByActiveBox.IsChecked ?? true;
            opt.SOPLikeName = FilterSOPByNameBox.Text;
            opt.SOPLikeVersion = FilterSOPByVersionBox.Text;
            opt.SOPLikeOwner = FilterSOPByOwnerBox.Text;
            opt.SOPLikeRenewal = renewal;

            // Get the query from the database
            QueriedLog = await Task.Run(async () =>
            {
                var x = await Task.Run(() => { return Database.GetTrainingDue(opt); });
                return x;
            });

            // Assign the results to the report grid
            ReportDataGrid.ItemsSource = QueriedLog;

            // Close the Busy Window
            win.Close();
        }

        private void PrintBtn_Click(object sender, RoutedEventArgs e)
        {
            // Exit if not query has been run
            if (QueriedLog == null || QueriedLog.Count < 1)
            {
                // Try to get results for a default query
                ApplyFilterBtn_Click(null, null);

                // If we still have nothing, give up
                if (QueriedLog == null || QueriedLog.Count < 1)
                    return;
            }

            string name = DateTime.Now.ToString("yyyy-MM-dd") + " -- Training Due";
            
            var win = new TrainingDueReportWindow(QueriedLog);
            win.ReportWindow.Title = name;
            win.ShowDialog();
        }
        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            if (QueriedLog == null || QueriedLog.Count == 0)
                return;

            System.Windows.Forms.SaveFileDialog Savedlg = new System.Windows.Forms.SaveFileDialog();
            Savedlg.Filter = "CSV Files (*.csv)|*.csv|All Files|*.*";
            Savedlg.Title = "Export to CSV file...";

            if (Savedlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string filename = Savedlg.FileName;
            List<string> file = new List<string>(QueriedLog.Count + 1);

            // Create the CSV
            
            // I should write a class to do this, this is dirty.  I feel dirty.  Reflection should make this pretty easy List<PropertyInfo> = typeof(t).GetProperties();

            // Header line
            file.Add("Employee.FirstName,Employee.LastName,Employee.Shift,Employee.Job,Employee.Active,SOP.Name,SOP.Version,SOP.Owner,SOP.Renewal,SOP.Active,TrainingDate");
            foreach (var t in QueriedLog)
            {
                // Each trainig log is a line
                StringBuilder line = new StringBuilder();

                line.Append(quote(t.Employee.FirstName));
                line.Append(',');
                line.Append(quote(t.Employee.LastName));
                line.Append(',');
                line.Append(quote(t.Employee.Shift));
                line.Append(',');
                line.Append(quote(t.Employee.Job));
                line.Append(',');
                line.Append(t.Employee.Active);
                line.Append(',');
                line.Append(quote(t.SOP.Name));
                line.Append(',');
                line.Append(quote(t.SOP.Version));
                line.Append(',');
                line.Append(quote(t.SOP.Owner));
                line.Append(',');
                line.Append(t.SOP.Renewal);
                line.Append(',');
                line.Append(t.SOP.Active);
                line.Append(',');
                line.Append(t.TrainingDate == null ? String.Empty : t.TrainingDate.Value.ToString("yyyy-MM-dd"));

                file.Add(line.ToString());
            }

            // Write this out to the selected file
            System.IO.File.WriteAllLines(filename, file);
        }
        private string quote(string s)
        {
            return '"' + s.Replace("\"", "\"\"") + '"';
        }

        // Show / Hide the Filter Panel
        private void FilterShowBtn_Checked(object sender, RoutedEventArgs e)
        {
            FilterPanel.Visibility = Visibility.Visible;
        }
        private void FilterShowBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterPanel.Visibility = Visibility.Collapsed;
        }
        // Show this page in fullscreen view
        private void FullscreenBtn_Click(object sender, RoutedEventArgs e)
        {
            // Hide the Fullscreen button so we cannot fullscreen from the fullscreen screen
            FullscreenBtn.Visibility = Visibility.Collapsed;

            // Launch the fullscreen window with this Page
            FullscreenWindow win = new FullscreenWindow(this);

            // Show the window
            win.Show();
        }
        // Clear the Filter input controls and rerun query
        private void ClearFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            FilterEEByFNameBox.Text = String.Empty;
            FilterEEByLNameBox.Text = String.Empty;
            FilterEEByShiftBox.SelectedIndex = 0;
            FilterEEByJobBox.SelectedIndex = 0;
            FilterEEByActiveBox.IsChecked = true;

            FilterSOPByNameBox.Text = String.Empty;
            FilterSOPByVersionBox.Text = String.Empty;
            FilterSOPByOwnerBox.Text = String.Empty;
            FilterSOPByRenewalBox.Text = "0";
            FilterSOPByActiveBox.IsChecked = true;

            ApplyFilterBtn_Click(null, null);
        }
    }
}
