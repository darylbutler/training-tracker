﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace KelseyTrainingApp
{
    /// <summary>
    /// Interaction logic for TrainingLogBySOPPage.xaml
    /// </summary>
    public partial class TrainingLogBySOPPage : Page
    {
        List<TrainingLog> Log;
        List<SOP> SOPs;
        List<SOP> filteredSOPs;

        SOP SelectedSOP;

        public TrainingLogBySOPPage()
        {
            InitializeComponent();

            // Load all the data
            SOPs = Database.GetAllSOPs();

            // Setup the filter link
            FilterByActiveBox.IsChecked = true;
            FilterByChanged(null, null);
        }

        // -- Find SOP Section --------------------------------------------------------------------
        private void FilterByChanged(object sender, TextChangedEventArgs e)
        {
            var tmpFiltered = from d in SOPs select d;

            if (!String.IsNullOrWhiteSpace(FilterByNameBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Name.StartsWith(FilterByNameBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByVersionBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Version.StartsWith(FilterByVersionBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByOwnerBox.Text))
                tmpFiltered = from d in tmpFiltered where d.Owner.StartsWith(FilterByOwnerBox.Text, true, null) select d;
            if (!String.IsNullOrWhiteSpace(FilterByRenewalBox.Text))
            {
                int newRenewal = -1;
                if (Int32.TryParse(FilterByRenewalBox.Text, out newRenewal))
                    tmpFiltered = from d in tmpFiltered where d.Renewal > newRenewal select d;
            }

            if (FilterByActiveBox.IsChecked ?? false)
                tmpFiltered = from d in tmpFiltered where d.Active == true select d;

            filteredSOPs = tmpFiltered.ToList<SOP>();
            FindSOPsDataGrid.ItemsSource = filteredSOPs;
        }
        private void FilterByActiveBox_Checked(object sender, RoutedEventArgs e)
        {
            FilterByChanged(sender, null);
        }

        private void FindSOPsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FindSOPsDataGrid.SelectedIndex > -1)
            {
                SelectedSOP = (SOP)FindSOPsDataGrid.SelectedItem;
                SelectedSOPLabel.Text = SelectedSOP.Name + ", ver " + SelectedSOP.Version + " :: " + SelectedSOP.Owner;
            }
        }

        // New Filter Stuff
        private void FilterByYearValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            // Only allow numbers
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void FilterDatesInYearBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            FilterDatesByYear.IsChecked = true;
        }
        private void FilterDateRange_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            FilterDatesByRange.IsChecked = true;
        }

        // Run the Query
        private void RunBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedSOP == null || SelectedSOP.ID < 1)
                return;

            // Check on Filters
            if (FilterDatesByYear.IsChecked ?? false)
            {
                if (String.IsNullOrWhiteSpace(FilterDatesInYearBox.Text) || FilterDatesInYearBox.Text.Length != 4)
                {
                    MessageBox.Show("When filtering by Year, the Year value cannot be empty and must be atleast 4 characters long!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }
            if (FilterDatesByRange.IsChecked ?? false)
            {
                // No dates selected
                if (!FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    MessageBox.Show("When filtering by Date Range, Atleast the Start or the End dates must have a value selected!", "Date Filter Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                // Single day query
                if (FilterDateRange_StartDate.SelectedDate.HasValue && !FilterDateRange_EndDate.SelectedDate.HasValue)
                {
                    FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                }
                else if (FilterDateRange_EndDate.SelectedDate.HasValue && !FilterDateRange_StartDate.SelectedDate.HasValue)
                {
                    FilterDateRange_StartDate.SelectedDate = FilterDateRange_EndDate.SelectedDate;
                }
                // End date before start date
                else if (FilterDateRange_StartDate.SelectedDate > FilterDateRange_EndDate.SelectedDate)
                {
                    var result = MessageBox.Show("The date selected as the starting date is later than the date selected as the ending date.\nDo you want to swap the dates?", "Dates are Inverted", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.No)
                        return;
                    else
                    {
                        DateTime? temp = FilterDateRange_EndDate.SelectedDate;
                        FilterDateRange_EndDate.SelectedDate = FilterDateRange_StartDate.SelectedDate;
                        FilterDateRange_StartDate.SelectedDate = temp;
                    }
                }
            }

            // Actually do the query
            if (FilterDatesAll.IsChecked ?? false)
                Log = Database.GetTrainingLogBySOP(ref SelectedSOP, (FilterByActiveEEs.IsChecked ?? true), null, null);
            else if (FilterDatesByYear.IsChecked ?? false)
            {
                DateTime startDate = new DateTime(Int32.Parse(FilterDatesInYearBox.Text), 1, 1);    // January 1st of the year
                DateTime endDate = startDate.AddDays(364);                                          // Not 365, because that would be Jan 1st of the next year

                Log = Database.GetTrainingLogBySOP(ref SelectedSOP, (FilterByActiveEEs.IsChecked ?? true), startDate, endDate);
            }
            else
            {
                Log = Database.GetTrainingLogBySOP(ref SelectedSOP, (FilterByActiveEEs.IsChecked ?? true),
                    FilterDateRange_StartDate.SelectedDate, FilterDateRange_EndDate.SelectedDate);
            }

            // Update the grid
            ReportDataGrid.ItemsSource = Log;
        }
        // Add SOP double click shortcut to run the query on that SOP
        private void FindSOPsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RunBtn_Click(null, null);
        }

        // Print Screenshot
        private void PrintBtn_Click(object sender, RoutedEventArgs e)
        {
            // Exit if not query has been run
            if (Log == null || Log.Count < 1)
                return;

            string name = DateTime.Now.ToString("yyyy-MM-dd") + " -- TrainingLog By " + SelectedSOP.Name;

            Log.First().SOP = SelectedSOP;    // Fill in the first sop for Report Viewer

            // Sort list
            Log = (from log in Log
                   orderby log.TrainingDate ascending, log.Employee.LastName ascending, log.Employee.FirstName ascending
                   select log).ToList();

            var win = new TrainingLogBySOPReportWindow(Log);
            win.ReportWindow.Title = name;
            win.ShowDialog();
        }

        // Export Report to csv for easy importing into Excel or a better report viewer
        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            // Exit if not query has been run
            if (Log == null || Log.Count < 1)
                return;

            string suggestedFileName = (DateTime.Now.ToString("yyyy-MM-dd") + " -- TrainingLog By " + SelectedSOP.Name).Replace('"', '\''); 

            System.Windows.Forms.SaveFileDialog Savedlg = new System.Windows.Forms.SaveFileDialog();
            Savedlg.FileName = suggestedFileName;
            Savedlg.Filter = "CSV Files (*.csv)|*.csv|All Files|*.*";
            Savedlg.Title = "Export to CSV file...";

            if (Savedlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return;

            string filename = Savedlg.FileName;
            List<string> file = new List<string>(Log.Count + 1);

            // Create the CSV
            // TODO Write a class to do this, this is dirty.  I feel dirty.  Reflection should make this pretty easy List<PropertyInfo> = typeof(t).GetProperties();

            // Header line
            file.Add("Training Date,Employee First Name,Employee Last Name,Employee Shift,Employee Job,Employee Is Active?");
            foreach (var t in Log)
            {
                // Each trainig log is a line
                StringBuilder line = new StringBuilder();

                line.Append(t.TrainingDate.HasValue ? ((DateTime)t.TrainingDate).ToString("yyyy-MM-dd") : String.Empty);
                line.Append(',');
                line.Append(quote(t.Employee.FirstName));
                line.Append(',');
                line.Append(quote(t.Employee.LastName));
                line.Append(',');
                line.Append(quote(t.Employee.Shift));
                line.Append(',');
                line.Append(quote(t.Employee.Job));
                line.Append(',');
                line.Append(t.Employee.Active);

                file.Add(line.ToString());
            }

            // Write this out to the selected file
            System.IO.File.WriteAllLines(filename, file);
        }
        private string quote(string s)
        {
            return '"' + s.Replace("\"", "\"\"") + '"';
        }

        // -- Delete Training Record
        private void ReportDataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DeleteBtn.IsEnabled = ReportDataGrid.SelectedIndex >= 0;
        }
        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            // Return if nothing selected
            if (ReportDataGrid.SelectedIndex < 0)
                return;

            // Prompt to delete
            if (MessageBox.Show("Are you sure you want to delete this training record?", "You Sure About This?",
                MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                return;

            // Call database to delete
            TrainingLog training_log = (TrainingLog)ReportDataGrid.SelectedItem;
            Database.DeleteTraining(ref training_log);

            // -- Update Bottom Panel
            int selected = ReportDataGrid.SelectedIndex;
            RunBtn_Click(null, null);
            if (selected > -1 && selected < ReportDataGrid.Items.Count)  // Restore same index if at beginning or middle of list
                ReportDataGrid.SelectedIndex = selected;
            else if (selected - 1 < ReportDataGrid.Items.Count)          // Restore last index if at end of list
                ReportDataGrid.SelectedIndex = selected - 1;
            else                                                         // Restore -1 because wtf
                ReportDataGrid.SelectedIndex = -1;

            // And Update the UI
            ReportDataGrid.Focus();
        }
    }
}

