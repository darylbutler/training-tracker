﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KelseyTrainingApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private Grid lastClickedGrid;

		public MainWindow()
		{
			InitializeComponent();
			// Set Window size
			MainAppWindow.Width = Properties.Settings.Default.MainWindowSize.Width;
			MainAppWindow.Height = Properties.Settings.Default.MainWindowSize.Height;
		}
		private void MainAppWindow_ContentRendered(object sender, EventArgs e)
		{
			// Put version info into the title bar
			this.Title = "Training Tracker v" + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;            			

			// Check if this is the first run
			// If so, select the database before we show the busy window
			bool chooseNewDB = false;
			if (Properties.Settings.Default.RemoteDatabasePath == String.Empty)
			{
				// No Path set
				MessageBox.Show("Remote Database Path isn't set.  Please locate the shared database file.", "First Run!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				chooseNewDB = true;
			}
			else if (!System.IO.File.Exists(Properties.Settings.Default.RemoteDatabasePath))
			{
				// Path to file not found
				MessageBox.Show("The saved Remote Database Path is pointing to a file that cannot be found. Please check the network connections if this file is remotely located.\n" +
								"In the following dialog, please attempt to locate the shared database file.  Pressing cancel will exit the application without saving any changes to the database path.",
								"Database Not Found!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				chooseNewDB = true;
			}

			// Prompt user for database location
			if (chooseNewDB)
			{
				using (System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog())
				{
					dlg.Filter = "Database Files (*.db)|*.db|All Files (*.*)|*.*";
					dlg.Title = "Select a Training Database";
					dlg.FileOk += (object sender2, System.ComponentModel.CancelEventArgs ce) =>
					{
						// Check if file is database
						if (!Database.DatabaseIsValid(dlg.FileName))
						{
							MessageBox.Show("This is not a valid database file.", "File not valid.", MessageBoxButton.OK, MessageBoxImage.Error);
							ce.Cancel = true;
						}
						if (!Database.DatabaseMatchesOurSchema(dlg.FileName))
						{
							MessageBox.Show("This is an old training database file and there exists no upgrade method.  Cannot open this file, please select another.", "Database not valid", MessageBoxButton.OK, MessageBoxImage.Error);
							ce.Cancel = true;
						}
					};

					if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
						Application.Current.Shutdown(); // If they cancel, we have no db to load, so exit application
					
					// Assign new Database path to settings, so we can open it by default next time
					Properties.Settings.Default.RemoteDatabasePath = dlg.FileName;
				}
			}       
			
			// Load the Database
			LoadDatabase();

			// Save new path if database load was successful and we had the user choose a new path
			if (chooseNewDB && Database.Loaded)
				Properties.Settings.Default.Save();
		}
		private async void SaveDatabaseBtn_Click(object sender, RoutedEventArgs e)
		{
			// Show busy window while saving the database
			BusyWindow win = new BusyWindow();
			win.WorkingLabel.Text = "Saving Database...";
			win.Owner = App.Current.MainWindow;
			win.Show();

			await Task.Run(async () =>
			{
				await Task.Run(() => { Database.SaveDatabase(); });
			});

			win.Close();
		}
		private void MainAppWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// Save Window Size
			Properties.Settings.Default.MainWindowSize = new System.Drawing.Size((int)MainAppWindow.Width, (int)MainAppWindow.Height);

			// Save any app settings that may have changed
			Properties.Settings.Default.Save();

			// Save the Database
			e.Cancel = !AskToSave();
		}
		private void mainFrame_ContentRendered(object sender, EventArgs e)
		{
			TitleTextBlock.Text = ((Page)mainFrame.Content).Title;
		}
		private void allBtnsClear(object sender)
		{
			if (lastClickedGrid != null)
				lastClickedGrid.Background = null;

			lastClickedGrid = ((sender as Button).Parent as Grid);
			lastClickedGrid.Background = new SolidColorBrush(Color.FromArgb(100, 157, 189, 198));

			// Shows the background right away
			mainFrame.Focus();
		}
		private void EditEmployeesBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new EditEmployeesPage());
			allBtnsClear(sender);
		}
		private void EditSOPsBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new EditSOPsPage());
			allBtnsClear(sender);
		}
		private void AddTrainingBySOPBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new AddTrainingBySOPPage());
			allBtnsClear(sender);
		}
		private void AddTrainingByEmployeeBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new AddTrainingByEmployeePage());
			allBtnsClear(sender);
		}
		private void TrainingDueBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new TrainingDuePage());
			allBtnsClear(sender);
		}
		private void TrainingMatrixBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new TrainingMatrixPage());
			allBtnsClear(sender);
		}
		private void TrainingMatrixSimpleBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new SimpleMatrixPage());
			allBtnsClear(sender);
		}
		private void EditShiftsBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new EditShiftsPage());
			allBtnsClear(sender);
		}
		private void EditJobsBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new EditJobsPage());
			allBtnsClear(sender);
		}
		private void TrainingLogByEmployeeBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new TrainingLogByEmployeePage());
			allBtnsClear(sender);
		}
		private void TrainingLogBySOPBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new TrainingLogBySOPPage());
			allBtnsClear(sender);
		}
		private void TrainingLogByDateBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new TrainingLogByDatePage());
			allBtnsClear(sender);
		}
		private void DatabaseBackupBtn_Click(object sender, RoutedEventArgs e)
		{
			mainFrame.Navigate(new BackupDatabasePage());
			allBtnsClear(sender);
		}
		// Checks whether the database has been modified since last save and asks to user to save or not
		private bool AskToSave()
		{
			// Save the Database
			if (Database.Modified)
			{
				var result = MessageBox.Show("Do you want to save the database back to the disk?\nIf you press No, any changes made in this session will be lost.", "Unsaved Changes!", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
				App.Current.MainWindow.Activate();
				if (result == MessageBoxResult.Cancel)
					return false;
				else if (result == MessageBoxResult.Yes)
					SaveDatabaseBtn_Click(this, null);
			}
			return true;
		}
		// Change database source
		private void ChangeDatabasePathBtn_Click(object sender, RoutedEventArgs e)
		{
			// Check for modified and Save
			if (!AskToSave())
				return;

			// Prompt user for database location                
			using (System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog())
			{
				dlg.Filter = "All Database Files (*.db)|*.db|All Files (*.*)|*.*";
				dlg.Title = "Select Database File";
				dlg.FileOk += (object sender2, System.ComponentModel.CancelEventArgs ce) =>
				{
					// Check if file is database
					if (!Database.DatabaseIsValid(dlg.FileName))
					{
						MessageBox.Show("This is not a valid database file.", "File not valid.", MessageBoxButton.OK, MessageBoxImage.Error);
						ce.Cancel = true;
					}
					if (!Database.DatabaseMatchesOurSchema(dlg.FileName))
					{
						MessageBox.Show("This is an old training database file and there exists no upgrade method.  Cannot open this file, please select another.", "Database not valid", MessageBoxButton.OK, MessageBoxImage.Error);
						ce.Cancel = true;
					}
				};

				if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
					return;

				// Load the new database
				Properties.Settings.Default.RemoteDatabasePath = dlg.FileName;
				LoadDatabase();
				// Save new path if database load was successful
				if (Database.Loaded)
					Properties.Settings.Default.Save();
			}
		}
		// Opens the default view when database is loaded
		private void LoadDefaultView()
		{
			int defView = Properties.Settings.Default.MainWindowDefaultOnOpen;

			switch(defView)
			{
				case 1:
					AddTrainingBySOPBtn_Click(AddTrainingBySOPBtn, null);
					break;
				case 2:
					AddTrainingByEmployeeBtn_Click(AddTrainingByEmployeeBtn, null);
					break;
				case 3:
					TrainingMatrixBtn_Click(TrainingMatrixBtn, null);
					break;
				case 4:
					TrainingMatrixSimpleBtn_Click(TrainingMatrixSimpleBtn, null);
					break;
				case 5:
					TrainingDueBtn_Click(TrainingDueBtn, null);
					break;
				case 6:
					TrainingLogByEmployeeBtn_Click(TrainingLogByEmployeeBtn, null);
					break;
				case 7:
					TrainingLogBySOPBtn_Click(TrainingLogBySOPBtn, null);
					break;
				case 8:
					TrainingLogByDateBtn_Click(TrainingLogByDateBtn, null);
					break;
				case 9:
					EditSOPsBtn_Click(EditSOPsBtn, null);
					break;
				default:
				case 10:
					EditEmployeesBtn_Click(EditEmployeesBtn, null);
					break;
				case 11:
					EditShiftsBtn_Click(EditShiftsBtn, null);
					break;
				case 12:
					EditJobsBtn_Click(EditJobsBtn, null);
					break;
			}
		}
		// Changes UI while Database loads database from saved path
		private async void LoadDatabase()
		{
			// Show Progress on Statusbar
			DatabasePathLabel.Text = "Loading Remote Database...";
			// Show busy window while preloading the database            
			BusyWindow win = new BusyWindow();
			win.WorkingLabel.Text = "Loading Database...";
			win.Owner = App.Current.MainWindow;
			win.Show();

			await Task.Run(async () =>
			{
				await Task.Run(() =>
				{
					// Load the Database
					Database.PreloadDatabase();
				});		
			});

			// Open to Default Page	   
			if (MainAppWindow.IsLoaded && Database.Loaded)
				LoadDefaultView();

			// Update Statusbar DB Path and return
			DatabasePathLabel.Text = Database.DatabasePath;
			App.Current.MainWindow.Activate();
			win.Close();						
		}
		// Launch project website
		private void IssueTrackLabel_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start("https://bitbucket.org/darylbutler/training-tracker");
		}
		// Create a new Database file
		private void CreateNewDatabaseBtn_Click(object sender, RoutedEventArgs e)
		{
			// Check for modified and Save
			if (!AskToSave())
				return;

			// Prompt user for database location                
			System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
			dlg.Filter = "All Database Files (*.db)|*.db|All Files (*.*)|*.*";
			dlg.Title = "Create New Database File";

			if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
				return;

			// Create the new database
			Database.CreateNewDatabase(dlg.FileName);

			// Save the new database path
			Properties.Settings.Default.RemoteDatabasePath = dlg.FileName;   // Switch to the new Database
			LoadDatabase();
			// Save new path if database load was successful
			if (Database.Loaded)
				Properties.Settings.Default.Save();
		}
	}
}
